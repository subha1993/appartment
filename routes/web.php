<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('home',function(){
    return  redirect('Admin/DashBoard');
});
Route::get('admin','Admin\Auth\LoginController@showLoginForm');
Route::get('admin/home','Admin\Auth\LoginController@showLoginForm');
Route::get('admin/login','Admin\Auth\LoginController@login')->name('login');
Route::post('admin/login','Admin\Auth\LoginController@login')->name('login');
Route::get('admin/logout','Admin\Auth\LoginController@logout');
Route::get('Admin/DashBoard','Admin\HomeController@index');
Route::get('Admin/Add/Property','Admin\HomeController@AddProperty');
Route::post('Admin/Property','Admin\addPropertyController@AddProperty')->name('AddProperty');

Route::get('/powner','Admin\Auth\RegisterController@showRegistrationForm');
Route::post('register','Admin\Auth\RegisterController@register')->name('register');
Route::get('Admin/propertylist','Admin\addPropertyController@propertylist');
Route::get('Admin/editproperty/{id}','Admin\addPropertyController@propertyedit');
Route::post('Admin/dltimage','Admin\addPropertyController@dltpropertyimage');
Route::post('Admin/updtProperty','Admin\addPropertyController@updtProperty')->name('UpdtProperty');
Route::get('Admin/dltProperty/{id}','Admin\addPropertyController@dltProperty');
Route::get('Admin/addamenity','Admin\HomeController@addamenity');
Route::post('Admin/addamenityprocess','Admin\HomeController@addamenityprocess');
Route::get('Admin/amenitylist','Admin\HomeController@amenitylist');
Route::get('Admin/editamenity/{id}','Admin\HomeController@editamenity');
Route::post('Admin/updtamenity','Admin\HomeController@updtamenity');
Route::get('Admin/dltamenity/{id}','Admin\HomeController@dltamenity');
Route::get('Admin/addlaundry','Admin\HomeController@addlaundry');
Route::get('Admin/laundrylist','Admin\HomeController@laundrylist');
Route::get('Admin/addpetamenity','Admin\HomeController@addpetamenity');
Route::get('Admin/petamenitylist','Admin\HomeController@petamenitylist');
Route::post('loadneighborhood','Admin\HomeController@loadneighbor');
Route::get('Admin/pricerange','Admin\HomeController@addPriceRange');
Route::post('Admin/addpricerange','Admin\HomeController@addPriceRangeProcess');
Route::get('Admin/pricerangelist','Admin\HomeController@pricerangelist');
Route::get('Admin/deletepricerange/{id}','Admin\HomeController@deletepricerange');
Route::get('Admin/contactpage','Admin\HomeController@contactform');
Route::post('Admin/addcontact','Admin\HomeController@addContact');
Route::get('Admin/contacts','Admin\HomeController@contactlist');
Route::get('Admin/editcontact/{id}','Admin\HomeController@editContact');
Route::post('Admin/updtcontact','Admin\HomeController@updtContact');
Route::get('Admin/dltcontact/{id}','Admin\HomeController@dltContact');
Route::get('Admin/smtp','Admin\HomeController@smtp');
Route::post('Admin/addsmtp','Admin\HomeController@addsmtp');
Route::get('Admin/addemailtemp','Admin\HomeController@emailtemplate');
Route::post('Admin/addemailtemplate','Admin\HomeController@addemailtemplate');
Route::get('Admin/emaillist','Admin\HomeController@emaillist');
Route::get('Admin/editemail/{id}','Admin\HomeController@editEmail');
Route::post('Admin/updtemail','Admin\HomeController@updtEmail');
Route::get('Admin/dltemail/{id}','Admin\HomeController@dltEmail');
Route::post('Admin/sendmanagermail','Admin\HomeController@sendmanagermail');
Route::get('Admin/scrapdata','Admin\addPropertyController@getscrapdata');
Route::get('Admin/addmanager','Admin\Usermanagement@addManager');
Route::post('Admin/addmanagerprocess','Admin\Usermanagement@addManagerProcess');
Route::get('Admin/blckdt/{id}','Admin\addPropertyController@blockdate');
Route::get('Admin/getblckdt','Admin\addPropertyController@getBlockDates');
Route::post('Admin/addblckdt','Admin\addPropertyController@addblockdate');
Route::get('Admin/fetchblckdt/{id}','Admin\addPropertyController@blockdatelist');
Route::post('Admin/dltblckdt','Admin\addPropertyController@deleteblckdt');

Route::get('verify/{email}/{verifyToken}','Admin\Auth\RegisterController@sendEmailDone')->name('sendEmailDone');
Route::get('User/list','Admin\Usermanagement@userlist');
Route::get('Property/Owner/Delete/{userid}','Admin\Usermanagement@userdelete');
Route::get('Property/Owner/Block/{userid}','Admin\Usermanagement@userblock');
Route::get('Property/Owner/Active/{userid}','Admin\Usermanagement@useractive');
Route::get('Property/Owner/Edit/{userid}','Admin\Usermanagement@useredit');
Route::post('Edit/Manager','Admin\Usermanagement@editManager')->name('editManager');
