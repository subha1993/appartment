<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'prefix' => '/'], function () {
    Route::post('propertylist', 'ApiController@propertylist');
    Route::post('propertydetails', 'ApiController@propertyDetails');
    Route::post('addfav', 'ApiController@addFav');
    Route::post('favlist', 'ApiController@Favlist');
    Route::post('managerlogin', 'ApiController@managerlogin');
    Route::post('managerapartment', 'ApiController@managerapartments');
    Route::post('searchapartment', 'ApiController@searchProperty');
    Route::post('managerlogout', 'ApiController@managerlogout');
    Route::post('enquiry', 'ApiController@enquiry');
    Route::get('neighborhood', 'ApiController@allneighborhood'); // old route
    Route::get('nbrhd', 'ApiController@neighborhoods'); //To fetch neighborhood
    Route::post('addreview', 'ApiController@addreview');
    Route::post('reviews', 'ApiController@reviews');
    Route::post('chatlist', 'ApiController@chatlist');
    Route::post('previouschat', 'ApiController@previouschat');
    Route::post('notification', 'ApiController@notifications');
    Route::post('forgetpassword', 'ApiController@forgetpassword');
    Route::get('resetpassword/{tkn}','ApiController@Resetpass');
    Route::post('resetpwd', 'ApiController@resetpassword');

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

});
