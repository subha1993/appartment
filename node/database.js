var mysql = require('mysql');
// var con = mysql.createConnection({
//     host: "localhost",
//     user: "vetredir_vet",
//     password: "g]kJmvSszNJT",
//     database: "vetredir_videocall"
// });

// con.connect(function (err) {
//     if (err) throw err;
// });

var con  = mysql.createPool({
    host     : 'apartmentapp.cy1qlg0arf5u.us-west-2.rds.amazonaws.com',
    user     : 'root',
    password : 'fRnMt1gxHkR3aMRmR9Yr',
    database : 'apartment'
});

con.getConnection(function(err, connection) {
    // connected! (unless `err` is set)
    if (err) throw err;
});

exports.fetching = function (table,conditions,parameters,orderBy,groupBy,limit,offset,callback) {
    var res="";
    var cond=" ";
    if(typeof conditions  !== 'undefined' && conditions !== ''){
        cond+="WHERE "+conditions;
    }
    var param="*";
    if(typeof parameters  !== 'undefined' && parameters !== ''){
        param=parameters;
    }
    var order=" ";
    if(typeof orderBy  !== 'undefined' && orderBy !== ''){
        order+="ORDER BY "+orderBy;
    }
    var group=" ";
    if(typeof groupBy  !== 'undefined' && groupBy !== ''){
        group+="GROUP BY "+groupBy;
    }
    var lim=" ";
    if(typeof limit  !== 'undefined' && limit !== ''){
        lim+="LIMIT "+limit;
    }
    var offs=" ";
    if(typeof offset  !== 'undefined' && offset !== ''){
        offs+="OFFSET "+offset;
    }
    var sql = "SELECT "+param+" FROM "+table+cond+group+order+lim+offs;
    con.query(sql, function (err, result, fields) {
        if (err) throw err;
        return callback(null,result);
    });
};


exports.inserting = function (table,values) {
    var sql = "INSERT INTO "+table+" SET ?";
    con.query(sql, values, function (err, result) {
        if (err) throw err;
    });
};


exports.updating = function (table,conditions,values) {
    var cond=" ";
    if(typeof conditions  !== 'undefined'){
        cond+="WHERE "+conditions;
    }
    var sql = "UPDATE "+table+" SET ?"+cond;
    con.query(sql, values, function (err, result) {
        if (err) throw err;
    });
};


exports.deleting = function(table,conditions) {
    var cond=" ";
    if(typeof conditions  !== 'undefined'){
        cond+="WHERE "+conditions;
    }
    var sql = "DELETE FROM "+table+cond;
    con.query(sql, function (err, result) {
        if (err) throw err;
    });
};