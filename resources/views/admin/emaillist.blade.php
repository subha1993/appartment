@extends('admin.layouts.app')
@section('pageTitle', 'Dashboard')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Email List
                <small>List</small>
            </h1>

            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Email List</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Emails</h3>
                        </div><!-- /.box-header -->

                        <div class="box-body">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <p>{!! $error !!}</p>
                                    @endforeach
                                </div>
                            @endif

                            @if(session('success'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {!! session('success') !!}
                                </div>
                            @endif


                            <table id="list_table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th> Title</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($emails as $email)

                                    <tr>
                                        <td>{{ $email->emailTitle }}</td>
                                        <td>
                                            <a href="{!! url('Admin/editemail/'.$email->id) !!}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <a href="{!! url('Admin/dltemail/'.$email->id) !!}" onclick="return confirm('Are you sure! you want to delete this Email?');" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Delete</a>
                                            {{--@if($allUser->is_active=='Y')--}}
                                            {{--<a href="{!! url('Property/Owner/Block/'.$allUser->id) !!}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Block</a>--}}
                                            {{--@else--}}
                                            {{--<a href="{!! url('Property/Owner/Active/'.$allUser->id) !!}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Active</a>--}}
                                            {{--@endif--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{--{{ $products->appends($_GET)->render() }}--}}
                            {{--{{ $allUsers->links() }}--}}
                        </div><!-- /.box-body -->
                        <div class="paginationDiv">
                        </div>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->

@endsection
