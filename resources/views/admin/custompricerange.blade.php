@extends('admin.layouts.app')
@section('pageTitle', 'Dashboard')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Add Price Range
            </h1>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="{!! url('Admin/DashBoard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Add Price Range</li>
            </ol>
        </section>


        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Price Range</h3>
                        </div>

                        @if($errors->any())

                            <div class="alert alert-danger">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                @foreach($errors->all() as $error)

                                    <p>{!! $error !!}</p>

                                @endforeach

                            </div>

                        @endif

                        @if(session('success'))

                            <div class="alert alert-success">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                {!! session('success') !!}

                            </div>

                    @endif

                    <!-- form start -->

                        <form class="form-horizontal" name="settings_form" method="post" enctype="multipart/form-data" action="{{url('Admin/addpricerange')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">

                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label"></label>
                                    <div class="col-sm-6">
                                        <label class="radio-inline">
                                            <input type="radio" name="status" value="g" onclick="showprc()">Greater than
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="status" value="l" onclick="showprc()">Less than
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="status" value="n" checked onclick="shownrml()">Normal
                                        </label>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="Property_Title" class="col-sm-2 control-label" id="minlbl">Min Price</label>
                                    <label for="Property_Title" class="col-sm-2 control-label" style="display: none" id="prclbl">Price</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="minprc" required name="minprice" value="" />
                                    </div>
                                </div>

                                <div class="form-group" id="maxdiv">
                                    <label for="Property_Title" class="col-sm-2 control-label">Max Price</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" required id="maxprc" name="maxprice" value="" />
                                    </div>
                                </div>

                            </div><!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div><!-- /.box-footer -->
                        </form>

                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
@endsection

@section('customScript')
    {{-- <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet" type="text/css" /> --}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaznxF-XOUJ4k-GDmXoULnzKE15zbEd74&libraries=places&language=ICL_LANGUAGE_CODE&callback=initMapNew" async defer></script>
    <script>
        function initMapNew(cc)
        {
            var inputt = document.getElementById('address2');
            var autocomplete = new google.maps.places.Autocomplete(inputt);
            autocomplete.addListener('place_changed', function()
            {
                var place = autocomplete.getPlace();
                //console.log(place);
                if (place.geometry.location)
                {
                    var lat = place.geometry.location.lat();
                    var lng = place.geometry.location.lng();
                    //alert(lat+'~~'+lng);
                    $("#lat2").val(lat);
                    $("#lng2").val(lng);
                }
                var componentForm = {
                    //street_number: 'short_name',
                    //route: 'long_name',
                    //administrative_area_level_1: 'short_name',
                    //country: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'long_name',
                    country: 'long_name',
                };
                var arrlen= Object.keys(place.address_components).length;
                var address = '';
                // Get each component of the address from the place details
                // and fill the corresponding field on the form.
                for (var i = 0; i < Object.keys(place.address_components).length; i++)
                {
                    var addressType = place.address_components[i].types[0];
                    //console.log(addressType);
                    if (componentForm[addressType])
                    {
                        var valu = place.address_components[i][componentForm[addressType]];
                        if(addressType=='administrative_area_level_1')
                        {
                            $('#state').val(valu);
                        }
                        if(addressType=='locality')
                        {
                            $('#city').val(valu);
                        }
                        if(addressType=='country')
                        {
                            $('#country').val(valu);
                        }
                    }
                }
                //console.log(Object.keys(place.address_components).length);
                //console.log(place.address_components);
            });
        }
        function holding()
        {
            var cat = $("#post_category").val();
            var title = $("#postTitle").val();
            var img = $("#logo").val();
            var price = $("#price").val();
            var address = $("#addressLine1").val();
            var city = $("#city").val();
            var country = $("#country").val();
            if(cat!='' && title!='' && img!='' && price!='' && address!='' && city!='' && country!='')
            {
                $("#postAdd").prop("disabled", true);
                $("#postAdd").val("Saving...");
                $("#submitform_frm").submit();
            }
        }
        arr = [8, 9, 10, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111];
        //console.log(arr);
        function hideprice(value)
        {
            //console.log(value);
            //console.log(arr.indexOf(parseInt(value)));
            if(arr.indexOf(parseInt(value))!= '-1')
            {
                $("#price").removeAttr('required');
                $("#price").val('1');
                $("#priceid").hide();
            }
            else
            {
                $("#priceid").show();
                $("#price").val('');
                $("#price").attr('required');
            }
        }
    </script>

    <script type="text/javascript">
        $(window).load(function () {
            $("#add").click(function(){
                var imagecount = $("#imagecount").val();
                if(imagecount < 10)
                {
                    var fieldItem = $('<div class="colM profilepicarea1"><label class="thumbimg"><input type="file" accept="image/*" name="add_images[]" class="more_image" value=""/><div class="addimg more_img"><span class="featured_text"><i class="fa fa-file-image-o"></i><span>Add Image</span></span></div></label><span class="remove"><i class="fa fa-close"></i></span></div>');
                    $(".addimgarea").append(fieldItem);
                    imagecount++;
                    $("#imagecount").val(imagecount);
                }
                else
                {
                    alert("Max image limit 10 ");
                }
            });
            $("body").on("click",".remove",function(){
                var imagecount = $("#imagecount").val();
                imagecount--;
                $("#imagecount").val(imagecount);
                $(this).parent(".colM").remove();
            });
        });

        var fimgID=0;
        var moreID = 0;
        $('#logo').change( function(event) {
            //var wrapper = $("#sel_img");
            var count_event = event.target.files.length;
            // alert(count_event);
            for (i = 0; i < count_event; i++) {
                var tmppath = URL.createObjectURL(event.target.files[i]);
                // alert(tmppath);
                var imgDet = $(this)[0].files[0];
                //var imgSize = URL.createObjectURL(event.target.files[i].sice);
                //console.log(imgDet);
                if(imgDet.size <= '5242880'){
                    $("#featured_img").empty();
                    $("#featured_img").css('background-image', 'url(' + tmppath + ')');
                    // alert(tmppath);
                    fimgID++;
                }
                else
                {
                    alert("Sorry, file size should not greater than 5 MB");
                }
            }
        });

        $('body').on("change", ".more_image", function(event)
        {
            //alert("ok");
            //var wrapper = $("#sel_img");
            var count_event = event.target.files.length;
            // alert(count_event);
            for (i = 0; i < count_event; i++)
            {
                var tmppath = URL.createObjectURL(event.target.files[i]);
                var imgDet = $(this)[0].files[0];
                //var imgSize = URL.createObjectURL(event.target.files[i].sice);
                //console.log(imgDet);
                if(imgDet.size <= '5242880'){
                    $(this).next(".more_img").empty();
                    $(this).next(".more_img").css('background-image', 'url(' + tmppath + ')');
                    // alert(tmppath);
                    fimgID++;
                }
                else
                {
                    alert("Sorry, file size should not greater than 5 MB");
                }
            }
        });

    </script>
    <script src="http://smartnet.com.hk/wp-content/themes/groupon/assets/ckeditor/ckeditor.js"></script>
    <script>
        $(document).ready(function() {
            CKEDITOR.replaceClass = 'texteditor';
        });
    </script>

    <script>
        $(window).load(function () {
            var dtpickCounter = 1;
            $("#add_block_time").click(function(){
                var Block_time = $("#Block_time").val();
                if(Block_time < 10)
                {
                    var fieldItem = $(
                        // '<div class="colM profilepicarea1"><label class="thumbimg">From <input type="datetime-local" name="from_datetime[]" value=""/> To <input type="datetime-local" name="to_datetime[]" value=""/></label>&nbsp &nbsp<span class="remove"><i class="fa fa-close"></i></span></div>'
                        '<div class="colM profilepicarea1">From<input type="text" class="datepicker" name="from_datetime[]" id="dateofpost1'+dtpickCounter+'" name="Date_of_Post" value="" style="max-width:200px;" placeholder="mm/dd/YYYY" required readonly />To<input type="text" name="to_datetime[]" class="datepicker" id="dateofpost2'+dtpickCounter+'" name="Date_of_Post" value="" style="max-width:200px;" placeholder="mm/dd/YYYY" required readonly /><span class="remove"><i class="fa fa-close"></i></span></div></br>'
                    );
                    $(".addblocktime").append(fieldItem);
                    $('.datepicker').datepicker({
                        autoclose: true
                    });
                    $('.datepicker').datepicker({
                        autoclose: true
                    });
                    dtpickCounter++;
                    Block_time++;
                    $("#Block_time").val(Block_time);
                }
                else
                {
                    alert("Max image limit 10 ");
                }
            });

            $("body").on("click",".remove",function(){
                var Block_time = $("#Block_time").val();
                Block_time--;
                $("#Block_time").val(Block_time);
                $(this).parent(".colM").remove();
            });
        });

        function showprc(){
            document.getElementById('maxdiv').style.display='none';
            document.getElementById('maxprc').value=0;
            document.getElementById('minlbl').style.display='none';
            document.getElementById('prclbl').style.display='block';

        }

        function shownrml(){
            document.getElementById('maxdiv').style.display='block';
            document.getElementById('maxprc').value='';
            document.getElementById('minlbl').style.display='block';
            document.getElementById('prclbl').style.display='none';

        }
    </script>

    <script>

    </script>
@endsection
