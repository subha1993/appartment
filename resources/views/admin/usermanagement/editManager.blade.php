@extends('admin.layouts.app')
@section('pageTitle', 'Dashboard')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Manager
                <small>Edit</small>
            </h1>

            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Edit</h3>
                        </div><!-- /.box-header -->

                        <div class="box-body">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <p>{!! $error !!}</p>
                                    @endforeach
                                </div>
                            @endif

                            @if(session('success'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {!! session('success') !!}
                                </div>
                            @endif
                            @foreach($userdatas as $userdata)
                                @endforeach
              <form class="form-horizontal" name="settings_form" method="post" enctype="multipart/form-data" action="{{route('editManager')}}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="userid" value="@if(isset($userdata->id)){{ $userdata->id }}@endif">

                  <div class="form-group">
                    <label for="Property_Title" class="col-sm-2 control-label">First Name*</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" required name="first_name" value="@if(isset($userdata->first_name)){{ $userdata->first_name }}@endif" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="Property_Title" class="col-sm-2 control-label">Last Name*</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" required name="last_name" value="@if(isset($userdata->last_name)){{ $userdata->last_name }}@endif" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="Property_Title" class="col-sm-2 control-label">Email*</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" required name="email" value="@if(isset($userdata->email)){{ $userdata->email }}@endif" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="Property_Title" class="col-sm-2 control-label">Phone*</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" required name="phone" value="@if(isset($userdata->phone_number)){{ $userdata->phone_number }}@endif" />
                    </div>
                  </div>
				  
				  <div class="form-group">
                    <label for="Property_Title" class="col-sm-2 control-label">Change Password</label>
                    <div class="col-sm-6">
                      <input type="password" class="form-control" id="pwd" name="password" placeholder="Password" />
                    </div>
                    <span><button onclick="show()" type="button">
                                <img src="https://i.stack.imgur.com/Oyk1g.png" alt="eye" id="eye"/>
                          </button>
                   </span>
                  </div>

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                  </div>
                </form>

                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
    <script>
             function show() {
               var a=document.getElementById("pwd");
               var b=document.getElementById("eye");
               if (a.type=="password")  {
                 a.type="text";
                 b.src="https://i.stack.imgur.com/waw4z.png";
               }
               else {
                 a.type="password";
                 b.src="https://i.stack.imgur.com/Oyk1g.png";
               }
             }
    </script>
@endsection
