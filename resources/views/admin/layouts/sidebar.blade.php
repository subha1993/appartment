<section class="sidebar">
	<!-- Sidebar user panel -->
	<div class="user-panel">
		<div class="pull-left image">
			<img src="{{ env('AWS_BUCKET_URL').'admin/dist/img/avatar5.png' }}" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
			<p>{{Auth::user()->first_name}}</p>
			<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	</div>
	<ul class="sidebar-menu">
		<li class="header">MAIN NAVIGATION</li>
		<li>
			<a href="{!! url('Admin/DashBoard') !!}">
				<i class="fa fa-dashboard"></i> <span>Dashboard</span>
			</a>
		</li>
		{{--<li class="">--}}
			{{--<a href="">--}}
				{{--<i class="fa fa-cog"></i> <span>Settings</span>--}}
			{{--</a>--}}
		{{--</li>--}}

        <!-- <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span> Banner Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> Banner Add </a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> Banner Edit </a>
                </li>
            </ul>
        </li> -->


		 <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Listings</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
					<a href="{!! url('Admin/propertylist') !!}"><i class="fa fa-circle-o"></i>All Listings</a>
                </li>
                <li class="">
                    <a href="{!! url('Admin/Add/Property') !!}"><i class="fa fa-circle-o"></i>Add new Listing</a>
                </li>
            </ul>
        </li>
        @if (Auth::user()->type != 2)
            <li class="">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Contact management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{!! url('Admin/contacts') !!}"><i class="fa fa-circle-o"></i>List Contact</a>
                    </li>
                    <li class="">
                        <a href="{!! url('Admin/contactpage') !!}"><i class="fa fa-circle-o"></i>Add Contact</a>
                    </li>
                </ul>
            </li>

            <li class="">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Email Template management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{!! url('Admin/emaillist') !!}"><i class="fa fa-circle-o"></i>List Email Template</a>
                    </li>
                    <li class="">
                        <a href="{!! url('Admin/addemailtemp') !!}"><i class="fa fa-circle-o"></i>Add Email Template</a>
                    </li>
                </ul>
            </li>

            <li class="">
                <a href="{!! url('Admin/smtp') !!}">
                    <i class="fa fa-user"></i>
                    <span>SMTP</span>
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                </a>
                {{--<ul class="treeview-menu">--}}
                    {{--<li class="">--}}
                        {{--<a href="{!! url('Admin/contacts') !!}"><i class="fa fa-circle-o"></i>List Contact</a>--}}
                    {{--</li>--}}
                    {{--<li class="">--}}
                        {{--<a href="{!! url('Admin/contactpage') !!}"><i class="fa fa-circle-o"></i>Add Contact</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            </li>
            @endif
@if (Auth::user()->type == 2)
			<li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Manager Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href="{!! url('User/list') !!}"><i class="fa fa-circle-o"></i>User List</a>
                    <a href="{!! url('Admin/addmanager') !!}"><i class="fa fa-circle-o"></i>Add Manager</a>
                </li>
                {{-- <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> Add</a>
                </li> --}}
            </ul>
        </li>

            <li class="">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Amenity management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{!! url('Admin/amenitylist') !!}"><i class="fa fa-circle-o"></i>List Amenity</a>
                    </li>
                    <li class="">
                        <a href="{!! url('Admin/addamenity') !!}"><i class="fa fa-circle-o"></i>Add Amenity</a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Unit Features management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{!! url('Admin/laundrylist') !!}"><i class="fa fa-circle-o"></i>List Unit Features</a>
                    </li>
                    <li class="">
                        <a href="{!! url('Admin/addlaundry') !!}"><i class="fa fa-circle-o"></i>Add Unit Features</a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Pet Amenity management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{!! url('Admin/petamenitylist') !!}"><i class="fa fa-circle-o"></i>Pet Amenities</a>
                    </li>
                    <li class="">
                        <a href="{!! url('Admin/addpetamenity') !!}"><i class="fa fa-circle-o"></i>Add Pet Amenity</a>
                    </li>
                </ul>
            </li>

            <li class="">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Price Range management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{!! url('Admin/pricerangelist') !!}"><i class="fa fa-circle-o"></i>List Price Range</a>
                    </li>
                    <li class="">
                        <a href="{!! url('Admin/pricerange') !!}"><i class="fa fa-circle-o"></i>Add Price Range</a>
                    </li>
                </ul>
            </li>
@endif
        {{--<li class="">--}}
            {{--<a href="#">--}}
                {{--<i class="fa fa-user"></i>--}}
                {{--<span>Block Dates management</span>--}}
                {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</a>--}}
            {{--<ul class="treeview-menu">--}}
                {{--<li class="">--}}
                    {{--<a href="{!! url('Admin/blckdt') !!}"><i class="fa fa-circle-o"></i>Add Blockdate</a>--}}
                {{--</li>--}}
                {{--<li class="">--}}
                    {{--<a href="{!! url('Admin/getblckdt') !!}"><i class="fa fa-circle-o"></i>Blockdate List</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
		<!--{{-- <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Category</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> List</a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> Add</a>
                </li>
            </ul>
        </li> --}}

		{{-- <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Product</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> List</a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> Add</a>
                </li>
            </ul>
        </li> --}}

		{{-- <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Brand</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> List</a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> Add</a>
                </li>
            </ul>
        </li> --}}

        {{-- <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Color</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> List</a>
                    <a href=""><i class="fa fa-circle-o"></i> Add</a>

                </li>
            </ul>
        </li> --}}

		{{-- <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Size</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> List</a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> Add</a>
                </li>
            </ul>
        </li> --}}
		{{-- <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Keyfeture</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> List</a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> Add</a>
                </li>
            </ul>
        </li> --}}
        {{-- <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>CashOut Request</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> List</a>
                </li>

            </ul>
        </li> --}}
        {{-- <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Shipping Methods</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> List </a>
                </li>
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> Add </a>
                </li>
            </ul>
        </li> --}}

        {{-- <li class="">
            <a href="#">
                <i class="fa fa-user"></i>
                <span> News Subscription</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href=""><i class="fa fa-circle-o"></i> List </a>
                </li>
            </ul>
        </li> --}}-->

        {{--<li class="">--}}
            {{--<a href="#">--}}
                {{--<i class="fa fa-user"></i>--}}
                {{--<span> Report Management</span>--}}
                {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</a>--}}
            {{--<ul class="treeview-menu">--}}
                {{--<li class="">--}}
                    {{--<a href=""><i class="fa fa-circle-o"></i> List </a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
		 {{--<li class="">--}}
            {{--<a href="#">--}}
                {{--<i class="fa fa-user"></i>--}}
                {{--<span> Help & Faq Categories</span>--}}
                {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</a>--}}
            {{--<ul class="treeview-menu">--}}
                {{--<li class="">--}}
                    {{--<a href=""><i class="fa fa-circle-o"></i> List </a>--}}
                {{--</li>--}}
                {{--<li class="">--}}
                    {{--<a href=""><i class="fa fa-circle-o"></i> Add </a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
		 {{--<li class="">--}}
            {{--<a href="#">--}}
                {{--<i class="fa fa-user"></i>--}}
                {{--<span> Help & Faq Posts</span>--}}
                {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</a>--}}
            {{--<ul class="treeview-menu">--}}
                {{--<li class="">--}}
                    {{--<a href=""><i class="fa fa-circle-o"></i> List </a>--}}
                {{--</li>--}}
                {{--<li class="">--}}
                    {{--<a href=""><i class="fa fa-circle-o"></i> Add </a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
	</ul>
</section>
