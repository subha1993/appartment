@extends('admin.layouts.app')

@section('pageTitle', 'Dashboard')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Unit Features
                <small>List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Unit Features</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">All Unit Features</h3>
                        </div><!-- /.box-header -->

                        {{--<div class="sortpr pull-right">--}}
                        {{--<form action="{!! admin_url('product-list') !!}" method="get" class="sortingForm">--}}
                        {{--<input type="hidden" name="keyword" value="{{ request()->input('keyword') }}">--}}
                        {{--<select name="sort_by" onchange="$('.sortingForm').submit()">--}}
                        {{--<option value="">Sort By</option>--}}
                        {{--<option value="name" {{ request()->input('sort_by')=='name' ? 'selected' : '' }}>Name</option>--}}
                        {{--<option value="name_desc" {{ request()->input('sort_by')=='name_desc' ? 'selected' : '' }}>Name(DESC)</option>--}}
                        {{--<option value="name_asc" {{ request()->input('sort_by')=='name_asc' ? 'selected' : '' }}>Name(ASC)</option>--}}

                        {{--</select>--}}
                        {{--</form>--}}
                        {{--</div>--}}
                        {{--<div class="headerSearchBox pull-right">--}}
                        {{--<form action="{!! admin_url('product-list') !!}" method="get">--}}
                        {{--<input type="text" placeholder="Search Product" class="field" name="keyword" value="{{ request()->input('keyword') }}" required="" id="search">--}}
                        {{--<input type="hidden" name="sort_by" value="{{ request()->input('sort_by') }}">--}}
                        {{--<button class="button pink" type="submit" id="searchList"><span>SEARCH</span></button>--}}
                        {{--</form>--}}
                        {{--</div>--}}
                        <div class="box-body">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <p>{!! $error !!}</p>
                                    @endforeach
                                </div>
                            @endif
                            @if(session('success'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table id="list_table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Logo</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($amenities as $amenity)

                                    <tr>
                                        <td>{{ $amenity->fields }}</td>
                                        @if($amenity->logo != '')
                                            <td><img style="height: 100px; width: 100px" class="blah" src="{{ env('AWS_BUCKET_URL').'admin/amenitylogo/'.$amenity->logo }}"></td>
                                        @else
                                            <td>{{$amenity->logo}}</td>
                                        @endif
                                        <td>
                                            <a href="{{url('Admin/editamenity/'.$amenity->id)}}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <a href="{{url('Admin/dltamenity/'.$amenity->id)}}" onclick="return confirm('Are you sure?')" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true" ></i> Delete</a>
                                        </td>
                                    </tr>
                                @endforeach



                                </tbody>
                            </table>
                            {{--{{ $products->appends($_GET)->render() }}--}}
                            {{--{{ $products->links() }}--}}
                        </div><!-- /.box-body -->
                        <div class="paginationDiv">

                        </div>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->

@endsection