@extends('admin.layouts.app')
@section('pageTitle', 'Dashboard')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Contact
                <small>Edit</small>
            </h1>

            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            {{--<h3 class="box-title">Add</h3>--}}
                        </div><!-- /.box-header -->

                        <div class="box-body">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <p>{!! $error !!}</p>
                                    @endforeach
                                </div>
                            @endif

                            @if(session('success'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <form class="form-horizontal" name="settings_form" method="post" enctype="multipart/form-data" action="{{url('Admin/updtcontact')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="cid" value="{{$contact->id}}">
                                <div class="form-group">
                                    <label for="Property_Title" class="col-sm-2 control-label">Name*</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" required name="name" value="{{$contact->name}}" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Property_Title" class="col-sm-2 control-label">Email*</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" required name="email" value="{{$contact->email}}" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Property_Title" class="col-sm-2 control-label">Phone</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="phone" value="{{$contact->phone}}" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Property_Title" class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" name="status" style="max-width:150px;">
                                            <option @if($contact->status == '1') selected @endif value="1">Active</option>
                                            <option @if($contact->status == '2') selected @endif value="2">Inactive</option>
                                            <option @if($contact->status == '3') selected @endif value="3">Unsubscribed</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">Save</button>
                                </div>
                            </form>

                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection
