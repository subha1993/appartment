@extends('admin.layouts.app')
@section('pageTitle', 'Dashboard')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Contact List
                <small>List</small>
            </h1>

            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Contact List</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Contacts</h3>
                            <button type="button" class="pull-right" onclick="tmpltslct()">Send Mail</button>
                        </div><!-- /.box-header -->

                        <div class="box-body">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <p>{!! $error !!}</p>
                                    @endforeach
                                </div>
                            @endif

                            @if(session('success'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {!! session('success') !!}
                                </div>
                            @endif


                            <table id="list_table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><input class="" id="mlchk" type="checkbox" value="" onchange="chkall()"></th>
                                    <th> Name</th>
                                    <th> Email</th>
                                    <th> Phone </th>
                                    <th> Status </th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($contact as $cntct)

                                    <tr>
                                        <td><input class="mailchk" type="checkbox" value="{{$cntct->id}}"></td>
                                        <td>{{ $cntct->name }}</td>
                                        <td>{{ $cntct->email }}</td>
                                        <td>{{ $cntct->phone }}</td>
                                        <td>@if($cntct->status=='1')Active
                                            @elseif ($cntct->status=='2')Inactive
                                            @elseif ($cntct->status=='3')Unsubscribed
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{!! url('Admin/editcontact/'.$cntct->id) !!}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <a href="{!! url('Admin/dltcontact/'.$cntct->id) !!}" onclick="return confirm('Are you sure! you want to delete this Contact?');" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Delete</a>
                                            {{--@if($allUser->is_active=='Y')--}}
                                                {{--<a href="{!! url('Property/Owner/Block/'.$allUser->id) !!}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Block</a>--}}
                                            {{--@else--}}
                                                {{--<a href="{!! url('Property/Owner/Active/'.$allUser->id) !!}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Active</a>--}}
                                            {{--@endif--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{--{{ $products->appends($_GET)->render() }}--}}
                            {{--{{ $allUsers->links() }}--}}
                        </div><!-- /.box-body -->
                        <div class="paginationDiv">
                        </div>
                    </div><!-- /.box -->
                </div>
            </div>
            <form action="{{url('Admin/sendmanagermail')}}" method="post" id="managermailform">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="users" id="users" value="">
                <input type="hidden" name="tmplt" id="tmplt" value="">
            </form>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                    <h4 class="modal-title">Select Email template</h4>
                </div>
                <div class="modal-body">
                  @foreach($emailtmplts as $emltmp)
                       {{$emltmp->emailTitle}} <input class="pull-right" type="radio" name="emltmplt" value="{{$emltmp->id}}"><br><br>
                  @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="sendmailtousrs()">Send</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('customScript')
    <script>
        function tmpltslct(){
          var n = $(".mailchk:checked").length;
          if(n == 0){
              alert('please select atleast one contact');
          }else{
              var choices = [];
              var els = document.getElementsByClassName('mailchk');
              for (var i=0;i<els.length;i++){
                  if ( els[i].checked ) {
                      choices.push(els[i].value);
                  }
              }
              document.getElementById('users').value=choices;
              $('#myModal').modal('show');
          }
        }

        function sendmailtousrs(){
            if ($("input:radio[name='emltmplt']:checked").length > 0){
                var tmp=$("input[name='emltmplt']:checked").val();
                document.getElementById('tmplt').value=tmp;
                $("#managermailform").submit();
            }else{
                alert('please select a template');
            }
        }

        function chkall(){
            var ischk=$("#mlchk").is(':checked');
            if(ischk) {
               $('.mailchk').prop("checked",true);
            }else{
                $('.mailchk').removeAttr("checked",true);
            }
        }
    </script>
@endsection