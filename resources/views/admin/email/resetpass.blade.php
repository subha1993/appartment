@component('mail::message')

    Hi {!! $usr->first_name !!},
    Your Reset Password link is <a href= "{!! $confirmlink !!}"> Click here </a>

    Thanks,
    {{ config('app.name') }}
@endcomponent