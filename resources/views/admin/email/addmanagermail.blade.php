@component('mail::message')

    Hi! {!! $user->first_name !!},

    You are now one of the manager of Apartmentapp.
    
    Credentials are:
    email: {!! $user->email !!}
    password: {!! $password !!}

    Thanks,
    {{ config('app.name') }}
@endcomponent