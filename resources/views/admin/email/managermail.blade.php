@component('mail::message')

    {!! $body->description !!}

    Thanks,
    {{ config('app.name') }}
@endcomponent