@extends('admin.layouts.app')
@section('pageTitle', 'Dashboard')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Block Dates Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="{!! url('Admin/DashBoard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Add Block Dates</li>
            </ol>
        </section>


        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Block Dates</h3>
                        </div>

                        @if($errors->any())

                            <div class="alert alert-danger">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                @foreach($errors->all() as $error)

                                    <p>{!! $error !!}</p>

                                @endforeach

                            </div>

                        @endif

                        @if(session('success'))

                            <div class="alert alert-success">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                {!! session('success') !!}

                            </div>

                    @endif

                    <!-- form start -->

                        <form class="form-horizontal" name="settings_form" method="post" enctype="multipart/form-data" action="{{url('Admin/addblckdt')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="Property_Title" class="col-sm-2 control-label">Property</label>
                                    <div class="col-sm-6">
                                {{--<select class="form-control" name="property" id="property">--}}
                                    {{--<option value="">--Choose Bedroom--</option>--}}
                                    {{--@foreach($property as $prpt)--}}
                                        {{--<option value="{{$prpt->id}}">{{$prpt->Property_title}}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                                        <label for="Property_Title" class="control-label">{{$property->Property_title}}</label>
                                        {{--{{$property->Property_title}}--}}
                                        <input type="hidden" name="property" value="{{$property->id}}">
                                    </div>
                                </div>

                                <div class="form-group">
                                <label for="Block_time" class="col-sm-2 control-label">Block time*</label>
                                <div class="col-sm-6">
                                <div class="addblocktime">
                                {{--@foreach($property['blockcount'] as $key)--}}
                                {{--<div class="colM profilepicarea1"><input type="text" class=" hasDatepicker form-control" name="from_datetime[]" id="dateofpost11" value="{{ str_replace("-","/",$key->from_datetime)}}-{{str_replace("-","/",$key->to_datetime)}}" style="max-width:200px;" placeholder="mm/dd/YYYY" required="" readonly=""><span class="remove"><i class="fa fa-close"></i></span></div>--}}
                                {{--@endforeach--}}
                                <div class="colM"><a class="" id="add_block_time"><div class="addimg"><span><i class="fa fa-plus"></i></span><p>Add Block Time</p></div></a></div>
                                </div>
                                </div>
                                </div>

                            </div><!-- /.box-body -->
                            <input type="hidden" name="imagecount" id="imagecount" value="0">
                            <input type="hidden" name="Block_time" id="Block_time" value="0">
                            <input type="hidden" name="postAddSubmit" value="postAddSubmit">

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div><!-- /.box-footer -->
                        </form>

                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
@endsection

@section('customScript')
    {{-- <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet" type="text/css" /> --}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaznxF-XOUJ4k-GDmXoULnzKE15zbEd74&libraries=places&language=ICL_LANGUAGE_CODE&callback=initMapNew" async defer></script>

    <script type="text/javascript">
        $(window).load(function () {
            $("#add").click(function(){
                var imagecount = $("#imagecount").val();
                if(imagecount < 10)
                {
                    var fieldItem = $('<div class="colM profilepicarea1" id="moreimg"><label class="thumbimg"><input type="file" accept="image/*" name="add_images[]" class="more_image" required value=""/><div class="addimg more_img"><span class="featured_text"><i class="fa fa-file-image-o"></i><span>Add Image</span></span></div></label><span class="remove"><i class="fa fa-close"></i></span></div>');
                    $(".addimgarea").append(fieldItem);
                    imagecount++;
                    $("#imagecount").val(imagecount);
                }
                else
                {
                    alert("Max image limit 10 ");
                }
            });
            $("body").on("click",".remove",function(){
                var imagecount = $("#imagecount").val();
                imagecount--;
                $("#imagecount").val(imagecount);
                $(this).parent(".colM").remove();
            });
        });

        var fimgID=0;
        var moreID = 0;
        $('#add_featured_image').change( function(event) {
            //var wrapper = $("#sel_img");
            var count_event = event.target.files.length;
            // alert(count_event);
            for (i = 0; i < count_event; i++) {
                var tmppath = URL.createObjectURL(event.target.files[i]);
                // alert(tmppath);
                var imgDet = $(this)[0].files[0];
                //var imgSize = URL.createObjectURL(event.target.files[i].sice);
                //console.log(imgDet);
                if(imgDet.size <= '5242880'){
                    $("#featured_img").empty();
                    $("#featured_img").css('background-image', 'url(' + tmppath + ')');
                    // alert(tmppath);
                    fimgID++;
                }
                else
                {
                    alert("Sorry, file size should not greater than 5 MB");
                }
            }
        });

        $('body').on("change", ".more_image", function(event)
        {
            //alert("ok");
            //var wrapper = $("#sel_img");
            var count_event = event.target.files.length;
            // alert(count_event);
            for (i = 0; i < count_event; i++)
            {
                var tmppath = URL.createObjectURL(event.target.files[i]);
                var imgDet = $(this)[0].files[0];
                //var imgSize = URL.createObjectURL(event.target.files[i].sice);
                //console.log(imgDet);
                if(imgDet.size <= '5242880'){
                    $(this).next(".more_img").empty();
                    $(this).next(".more_img").css('background-image', 'url(' + tmppath + ')');
                    // alert(tmppath);
                    fimgID++;
                }
                else
                {
                    alert("Sorry, file size should not greater than 5 MB");
                }
            }
        });

    </script>
    <script src="http://smartnet.com.hk/wp-content/themes/groupon/assets/ckeditor/ckeditor.js"></script>
    <script>
        $(document).ready(function() {
            CKEDITOR.replaceClass = 'texteditor';
        });
    </script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>
        $(window).load(function () {
            var dtpickCounter = 1;
            $("#add_block_time").click(function(){
                var Block_time = $("#Block_time").val();
                if(Block_time < 10)
                {
                    var fieldItem = $(
                        //'<div class="colM profilepicarea1"><label class="thumbimg">From <input type="datetime-local" name="from_datetime[]" value=""/> To <input type="datetime-local" name="to_datetime[]" value=""/></label>&nbsp &nbsp<span class="remove"><i class="fa fa-close"></i></span></div>'
                        '<div class="column form-group"><input type="text" class="datepicker form-control" style="max-width:530px;float: left;" name="from_datetime[]" id="dateofpost'+dtpickCounter+'" name="Date_of_Post" value="" placeholder="mm/dd/YYYY" required readonly /><span class="remove1"><i class="fa fa-close"></i></span></div>'
                    );
                    $(".addblocktime").append(fieldItem);
                    $('.datepicker').daterangepicker({
                        timePicker: true,
                        startDate: moment().startOf('hour'),
                        endDate: moment().startOf('hour').add(30, 'day'),
                        locale: {
                            format: 'Y-M-DD hh:mm A'
                        }
                    }, function(start, end, label) {
                        if(((end-start)/(24 * 60 * 60 * 1000))<30){
                            alert("you added "+(end-start)/(24 * 60 * 60 * 1000)+" days");
                        }
                    });
                    dtpickCounter++;
                    Block_time++;
                    $("#Block_time").val(Block_time);
                }
                else
                {
                    alert("Max image limit 10 ");
                }
            });

            $("body").on("click",".remove1",function(){
                var Block_time = $("#Block_time").val();
                Block_time--;
                $("#Block_time").val(Block_time);
                $(this).parent(".column").remove();
            });
        });

        function chklatlng(){
            var lat=document.getElementById('lat2').value;
            var lng=document.getElementById('lng2').value;
            if(lat === ''){
                alert('please select Location from dropdown list');
                return false;
            }else if(lng === ''){
                alert('please select Location from dropdown list');
                return false;
            } else{
                return true;
            }
        }
    </script>
@endsection
