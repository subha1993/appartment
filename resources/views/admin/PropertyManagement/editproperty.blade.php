@extends('admin.layouts.app')
@section('pageTitle', 'Dashboard')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Property Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="{!! url('Admin/DashBoard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Edit Property</li>
            </ol>
        </section>


        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Property</h3>
                        </div>

                        @if($errors->any())

                            <div class="alert alert-danger">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                @foreach($errors->all() as $error)

                                    <p>{!! $error !!}</p>

                                @endforeach

                            </div>

                        @endif

                        @if(session('success'))

                            <div class="alert alert-success">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                {!! session('success') !!}

                            </div>

                    @endif

                    <!-- form start -->

                        <form class="form-horizontal" name="settings_form" method="post" enctype="multipart/form-data" action="{{route('UpdtProperty')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">

                                <div class="form-group">
                                    <label for="Property_Title" class="col-sm-2 control-label">Property Owner</label>
                                    <div class="col-sm-6">
                                        @if(Auth::user()->type == 2)
                                        <select class="form-control" name="owner" id="owner">
                                            {{--<option value="">--Choose Bedroom--</option>--}}
                                            @foreach($users as $user)
                                            <option @if($property->propertyuser['id'] == $user->id) selected @endif value="{{$user->id}}">@if($user->type == '2') {{$user->first_name}} @else {{$user->first_name}} {{$user->last_name}} @endif</option>
                                            @endforeach
                                        </select>
                                        @else
                                        {{--@if($property->propertyuser['type'] == 2)--}}
                                            {{--{{$property->propertyuser['first_name']}}--}}
                                        {{--@else--}}
                                            {{$property->propertyuser['first_name']}} {{$property->propertyuser['last_name']}}
                                            <input type="hidden" name="owner" value="{{$property->propertyuser['id']}}">
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Property_Title" class="col-sm-2 control-label">Property Title*</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" required name="Property_Title" value="{{$property->Property_title}}" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Property_Description" class="col-sm-2 control-label">Property Description*</label>
                                    <div class="col-sm-6">
                                        <textarea name="Property_Description" required id="postContent" class="texteditor" rows="4" cols="83">{{$property->Property_Description}}</textarea>
                                    </div>
                                </div>

                                {{--<div class="form-group">--}}
                                    {{--<label for="Price" class="col-sm-2 control-label">Price*</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{-- <input type="text" class="form-control" id="site_title" name="site_title" value="" /> --}}
                                        {{--<select name="price_currency_type" class="form-control" style="max-width:150px;">--}}
                                            {{--<option @if($property->price_currency_type == 'US$') selected @endif value="US$"> US$ </option>--}}
                                            {{--<option @if($property->price_currency_type == 'HK$') selected @endif value="HK$"> HK$ </option>--}}
                                            {{--<option @if($property->price_currency_type == 'EUR') selected @endif value="EUR"> EUR </option>--}}
                                        {{--</select>--}}
                                        {{--<input name="price" class="form-control" required type="text" value="{{$property->price}}" style="max-width:150px;"/>--}}
                                        {{-- <select style="width:20%;" name="unit_price">--}}
                                             {{--<option value="/item">/item</option>--}}
                                             {{--<option value="/ft">/ft</option>--}}
                                             {{--<option value="/cm">/cm</option>--}}
                                             {{--<option value="/sq ft">/sq ft</option>--}}
                                        {{--</select> --}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label for="Number_of_Bedrooms" class="col-sm-2 control-label">Bedrooms*</label>
                                    <div class="col-sm-6">
                                        {{--<input type="text" class="form-control" required name="Number_of_Bedrooms" value="{{$property->Bedrooms}}" style="max-width:150px;"/>--}}
                                        <select class="form-control" name="Number_of_Bedrooms" id="Number_of_Bedrooms">
                                            {{--<option value="">--Choose Bedroom--</option>--}}
                                            <option @if($property->Bedrooms == 'studio') selected @endif value="studio">studio</option>
                                            <option @if($property->Bedrooms == '1') selected @endif value="1">1</option>
                                            <option @if($property->Bedrooms == '2') selected @endif value="2">2</option>
                                            <option @if($property->Bedrooms == '3') selected @endif value="3">3</option>
                                            <option @if($property->Bedrooms == '4+') selected @endif value="4+">4+</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Number_of_Bathroom" class="col-sm-2 control-label">Number of Bathroom*</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" required name="Number_of_Bathroom" value="{{$property->Bathroom}}" style="max-width:150px;"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Location" class="col-sm-2 control-label">Location*</label>
                                    <div class="col-sm-6">
                                        <div class="fld">
                                            <input id="address2" class="form-control" required name="Location" required type="text" placeholder="Where? (or search)" class="txtFld" value="{{$property->Location}}" autocomplete="off">
                                            <input id="lat2" name="addr_lat" type="hidden" value="{{$property->loc_lat}}">
                                            <input id="lng2" name="addr_long" type="hidden" value="{{$property->loc_long}}">
                                        </div>
                                    </div>
                                </div>

                                {{--<div class="form-group">--}}
                                    {{--<label for="City" class="col-sm-2 control-label">City*</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<input type="text" class="form-control" id="city" required name="City"  value="{{$property->city}}">--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label for="Country" class="col-sm-2 control-label">Country*</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<select class="form-control" name="Country" id="country">--}}
                                            {{--<option value="">--Choose Country--</option>--}}
                                            {{--@foreach($countries as $country)--}}
                                                {{--<option @if($property->country == $country->name) selected @endif value="{{ $country->name }}" @if(old('country_id')==$country->id) selected @endif>{{ $country->name }}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label for="Country" class="col-sm-2 control-label">Borough*</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<select class="form-control" name="borough" id="borogh" onchange="nbrhood(this.value)">--}}
                                            {{--<option value="">--Choose Borough--</option>--}}
                                            {{--@foreach($borrogh as $brgh)--}}
                                                {{--<option value="{{ $brgh->id }}" @if($property->borough==$brgh->id) selected @endif>{{ $brgh->name }}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label for="Country" class="col-sm-2 control-label">Neighborhood*</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" name="neighborhood" id="neighbor">
                                            <option value="">--Choose Neighborhood--</option>
                                            @foreach($neighborhood as $neighbor)
                                                <option value="{{ $neighbor->id }}" @if($property->neighborhood==$neighbor->id) selected @endif>{{ $neighbor->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Address1" class="col-sm-2 control-label">Display Address*</label>
                                    <div class="col-sm-6">
                                        {{-- <input type="text" class="form-control" name="Address1" value="" /> --}}
                                        <textarea name="Address1" required class="form-control" rows="4" cols="83">{{$property->Full_Address1}}</textarea>
                                    </div>
                                </div>

                                {{--<div class="form-group">--}}
                                    {{--<label for="Address2" class="col-sm-2 control-label">Address 2</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{-- <input type="text" class="form-control" name="Address1" value="" /> --}}
                                        {{--<textarea name="Address2" class="form-control" rows="4" cols="83">{{$property->Full_Address2}}</textarea>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label for="zipcode" class="col-sm-2 control-label">Display Address</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<input type="text" class="form-control" name="display_address" value="{{$property->display_address}}" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label for="zipcode" class="col-sm-2 control-label">Zipcode*</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<input type="text" required class="form-control" name="zipcode" value="{{$property->zipcode}}" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label for="Rent" class="col-sm-2 control-label">Rent</label>
                                    <div class="col-sm-6">
                                        {{--<select name="Rent_currency_type" class="form-control" style="max-width:150px;">--}}
                                            {{--<option @if($property->price_currency_type == 'US$') selected @endif value="US$"> US$ </option>--}}
                                            {{--<option @if($property->price_currency_type == 'HK$') selected @endif value="HK$"> HK$ </option>--}}
                                            {{--<option @if($property->price_currency_type == 'EUR') selected @endif value="EUR"> EUR </option>--}}
                                        {{--</select>--}}

                                        <input name="Rent" class="" required type="text" value="{{$property->Rent}}" style="max-width:150px;"/>/month
                                        {{--<select class="form-control" name="unit_rent" style="max-width:150px;">--}}
                                            {{--<option @if($property->unit_rent == '/day' || $property->unit_rent == 'per day') selected @endif value="per day">/day</option>--}}
                                            {{--<option @if($property->unit_rent == '/month' || $property->unit_rent == 'per month') selected @endif value="per month">/month</option>--}}
                                            {{--<option @if($property->unit_rent == '/year' || $property->unit_rent == 'per year') selected @endif value="per year">/year</option>--}}
                                        {{--</select>--}}
                                    </div>
                                </div>

                                {{--<div class="form-group">--}}
                                    {{--<label for="Bills" class="col-sm-2 control-label">Bills</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<input type="text" class="form-control" name="Bills" value="{{$property->Bills}}" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label for="Deposit" class="col-sm-2 control-label">Deposit</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<input type="text" class="form-control" name="Deposit" value="{{$property->Deposit}}" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label for="Room_size" class="col-sm-2 control-label">Room size</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<input type="text" class="form-control" name="Room_size" value="{{$property->Room_size}}" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label for="Block_time" class="col-sm-2 control-label">Block time*</label>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<div class="addblocktime">--}}
                                            {{--@foreach($property['blockcount'] as $key)--}}
                                                {{--<div class="colM profilepicarea1"><input type="text" class=" hasDatepicker form-control" name="from_datetime[]" id="dateofpost11" value="{{ str_replace("-","/",$key->from_datetime)}}-{{str_replace("-","/",$key->to_datetime)}}" style="max-width:200px;" placeholder="mm/dd/YYYY" required="" readonly=""><span class="remove"><i class="fa fa-close"></i></span></div>--}}
                                            {{--@endforeach--}}
                                            {{--<div class="colM"><a class="" id="add_block_time"><div class="addimg"><span><i class="fa fa-plus"></i></span><p>Add Block Time</p></div></a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            @php $amntaray=array(); @endphp
                                @foreach($property['propertyamenity'] as $key)
                                    @php
                                        array_push($amntaray,$key->amenity_id);
                                    @endphp
                                    @endforeach

                                <div class="form-group">
                                    {{-- @if(isset($amenity->root_id)) --}}
                                    <label for="Amenities" class="col-sm-2 control-label">Amenities</label>
                                    <div class="col-sm-6">

                                        <table class="table">
                                            @foreach($amenities as $amenity)
                                                @if($amenity->root_id==null)
                                                    <thead>
                                                    <tr>
                                                        <th scope="col"><h2>{{ $amenity->fields }}</h2></th>
                                                    </tr>
                                                    </thead>
                                                    @foreach($amenities as $subamenity)
                                                        @if($subamenity->root_id==$amenity->id)
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input class="@if($amenity->id == 2) chkrdo @elseif($amenity->id == 3) chkrdo1 @endif" id="@php if($amenity->id == 3 && $subamenity->fields == 'No pets allowed'){ echo 'nopet'; }@endphp" @if(in_array($subamenity->id, $amntaray)) checked @endif  type="checkbox" name="amenities[{{ $amenity->id }}][{{ $subamenity->id }}]" value="{{ $subamenity->id }}" check-val="{{ $subamenity->fields }}"/>{{ $subamenity->fields }}
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </table>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Image" class="col-sm-2 control-label">Image</label>
                                    @foreach($property['images'] as $key)
                                    <div class="colM profilepicarea1" id="img{{$key->id}}"><label class="thumbimg"><img style="height: 100px; width: 100px" class="blah" src="{{ env('AWS_BUCKET_URL').'admin/PropertyManagement/image/'.$key->image_name }}"></label><span class=""><a href="javascript:void(0)" onclick="dltImg({{$key->id}})"><i class="fa fa-close"></i></a></span></div>
                                    @endforeach
                                    {{--<div class="col-sm-6">--}}
                                        {{--<div class="fld profilepicarea1"><label class="thumbimg">--}}
                                                {{--<input type="file" name="add_featured_image" accept="image/*" id="add_featured_image" class="add_featured_image" />--}}
                                                {{--<!-- <input type="file" required name="add_featured_image" id="add_featured_image" class="add_featured_image" /> -->--}}
                                                {{--<div id="featured_img" class="addimg">--}}
                    {{--<span class="featured_text">--}}
                      {{--<i class="fa fa-file-image-o"></i>--}}
                      {{--<span>Add Image*</span>--}}
                    {{--</span>--}}
                                                {{--</div>--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>

                                <div class="form-group">
                                    <label for="More_Image" class="col-sm-2 control-label"><h4>More Image</h4></label>
                                    <div class="colM profilepicarea1"><label class="thumbimg"><input type="file" accept="image/*" name="add_images[]" multiple class="more_image" value=""/><div class="addimg more_img"><span class="featured_text"><i class="fa fa-file-image-o"></i></span></div></label></div>
                                    {{--<div class="col-sm-6">--}}
                                        {{--<div class="addimgarea">--}}
                                            {{--<div class="colM"><a class="thumbimg" id="add"><div class="addimg"><span><i class="fa fa-plus"></i></span><p>Add Image</p></div></a></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                                <label for="More_Image" class="col-sm-5 control-label">Please upload max 10 image at a time and make total size of 25 mb</label>

                            </div><!-- /.box-body -->
                            <input type="hidden" name="imagecount" id="imagecount" value="{{count($property['images'])}}">
                            <input type="hidden" name="Block_time" id="Block_time" value="{{count($property['blockcount'])}}">
                            <input type="hidden" name="postAddSubmit" value="postAddSubmit">
                            <input type="hidden" name="prid" value="{{$property->id}}">

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div><!-- /.box-footer -->
                        </form>

                    </div><!-- /.box -->
                </div>
            </div>
        </section>
    </div>
@endsection

@section('customScript')
    {{-- <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet" type="text/css" /> --}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaznxF-XOUJ4k-GDmXoULnzKE15zbEd74&libraries=places&language=ICL_LANGUAGE_CODE&callback=initMapNew" async defer></script>
    <script>
        function initMapNew(cc)
        {
            var inputt = document.getElementById('address2');
            var autocomplete = new google.maps.places.Autocomplete(inputt);
            autocomplete.addListener('place_changed', function()
            {
                var place = autocomplete.getPlace();
                //console.log(place);
                if (place.geometry.location)
                {
                    var lat = place.geometry.location.lat();
                    var lng = place.geometry.location.lng();
                    //alert(lat+'~~'+lng);
                    $("#lat2").val(lat);
                    $("#lng2").val(lng);
                }
                var componentForm = {
                    //street_number: 'short_name',
                    //route: 'long_name',
                    //administrative_area_level_1: 'short_name',
                    //country: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'long_name',
                    country: 'long_name',
                };
                var arrlen= Object.keys(place.address_components).length;
                var address = '';
                // Get each component of the address from the place details
                // and fill the corresponding field on the form.
                for (var i = 0; i < Object.keys(place.address_components).length; i++)
                {
                    var addressType = place.address_components[i].types[0];
                    //console.log(addressType);
                    if (componentForm[addressType])
                    {
                        var valu = place.address_components[i][componentForm[addressType]];
                        if(addressType=='administrative_area_level_1')
                        {
                            $('#state').val(valu);
                        }
                        if(addressType=='locality')
                        {
                            $('#city').val(valu);
                        }
                        if(addressType=='country')
                        {
                            $('#country').val(valu);
                        }
                    }
                }
                //console.log(Object.keys(place.address_components).length);
                //console.log(place.address_components);
            });
        }
        function holding()
        {
            var cat = $("#post_category").val();
            var title = $("#postTitle").val();
            var img = $("#add_featured_image").val();
            var price = $("#price").val();
            var address = $("#addressLine1").val();
            var city = $("#city").val();
            var country = $("#country").val();
            if(cat!='' && title!='' && img!='' && price!='' && address!='' && city!='' && country!='')
            {
                $("#postAdd").prop("disabled", true);
                $("#postAdd").val("Saving...");
                $("#submitform_frm").submit();
            }
        }
        arr = [8, 9, 10, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111];
        //console.log(arr);
        function hideprice(value)
        {
            //console.log(value);
            //console.log(arr.indexOf(parseInt(value)));
            if(arr.indexOf(parseInt(value))!= '-1')
            {
                $("#price").removeAttr('required');
                $("#price").val('1');
                $("#priceid").hide();
            }
            else
            {
                $("#priceid").show();
                $("#price").val('');
                $("#price").attr('required');
            }
        }
    </script>

    <script type="text/javascript">
        $(window).load(function () {
            $("#add").click(function(){
                var imagecount = $("#imagecount").val();
                if(imagecount < 10)
                {
                    var fieldItem = $('<div class="colM profilepicarea1"><label class="thumbimg"><input type="file" accept="image/*" name="add_images[]" class="more_image" value=""/><div class="addimg more_img"><span class="featured_text"><i class="fa fa-file-image-o"></i><span>Add Image</span></span></div></label><span class="remove"><i class="fa fa-close"></i></span></div>');
                    $(".addimgarea").append(fieldItem);
                    imagecount++;
                    $("#imagecount").val(imagecount);
                }
                else
                {
                    alert("Max image limit 10 ");
                }
            });
            $("body").on("click",".remove",function(){
                var imagecount = $("#imagecount").val();
                imagecount--;
                $("#imagecount").val(imagecount);
                $(this).parent(".colM").remove();
            });
        });

        var fimgID=0;
        var moreID = 0;
        $('#add_featured_image').change( function(event) {
            //var wrapper = $("#sel_img");
            var count_event = event.target.files.length;
            // alert(count_event);
            for (i = 0; i < count_event; i++) {
                var tmppath = URL.createObjectURL(event.target.files[i]);
                // alert(tmppath);
                var imgDet = $(this)[0].files[0];
                //var imgSize = URL.createObjectURL(event.target.files[i].sice);
                //console.log(imgDet);
                if(imgDet.size <= '5242880'){
                    $("#featured_img").empty();
                    $("#featured_img").css('background-image', 'url(' + tmppath + ')');
                    // alert(tmppath);
                    fimgID++;
                }
                else
                {
                    alert("Sorry, file size should not greater than 5 MB");
                }
            }
        });

        $('body').on("change", ".more_image", function(event)
        {
            var count_event = event.target.files.length;
            if(count_event > 10){
                alert('You can not upload more than 10 images');
                $(this).val('');
            }
            else {

                var sz = 0;
                for (var i = 0; i < count_event; i++) {
                    sz += $(this)[0].files[i].size;
                }
                console.log(sz);
                //if(sz > 10485760){
                if(sz > 26214400){ //25MB
                    alert('Make all the files total size within 25MB');
                    $(this).val('');
                }
            }
//            //alert("ok");
//            //var wrapper = $("#sel_img");
//            var count_event = event.target.files.length;
//            // alert(count_event);
//            for (i = 0; i < count_event; i++)
//            {
//                var tmppath = URL.createObjectURL(event.target.files[i]);
//                var imgDet = $(this)[0].files[0];
//                //var imgSize = URL.createObjectURL(event.target.files[i].sice);
//                //console.log(imgDet);
//                if(imgDet.size <= '5242880'){
//                    $(this).next(".more_img").empty();
//                    $(this).next(".more_img").css('background-image', 'url(' + tmppath + ')');
//                    // alert(tmppath);
//                    fimgID++;
//                }
//                else
//                {
//                    alert("Sorry, file size should not greater than 5 MB");
//                }
//            }
        });

        function nbrhood(id){
//      if(id===""){
//       alert('hi');
//      }else {
//          alert(id);
//      }
//      return false;
            $.ajax({
                url:"{{ url('loadneighborhood') }}",
                type:"POST",
                data: {
                    id:id
                },
                success:function (result) {
                    $('#neighbor').html(result);
                }
            });
        }

    </script>
    <script src="http://smartnet.com.hk/wp-content/themes/groupon/assets/ckeditor/ckeditor.js"></script>
    <script>
        $(document).ready(function() {
            CKEDITOR.replaceClass = 'texteditor';
        });
    </script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>
        $(window).load(function () {
            var dtpickCounter = 1;
            $("#add_block_time").click(function(){
                var Block_time = $("#Block_time").val();
                if(Block_time < 10)
                {
                    var fieldItem = $(
                        '<div class="colM profilepicarea1"><input type="text" class="datepicker form-control" name="from_datetime[]" id="dateofpost1'+dtpickCounter+'" name="Date_of_Post" value="" style="max-width:1000px;" placeholder="mm/dd/YYYY" required readonly /><span class="remove"><i class="fa fa-close"></i></span></div>'
                    );
                    $(".addblocktime").append(fieldItem);
                    $('.datepicker').daterangepicker({
                             timePicker: false,    startDate: moment().startOf('hour'),    endDate: moment().startOf('hour').add(30, 'day'),    locale: {      format: 'D/M/Y'    }      }, function(start, end, label) {    if(((end-start)/(24 * 60 * 60 * 1000))<30){      alert("you added "+parseInt((end-start)/(24 * 60 * 60 * 1000))+" days");    }
                    });
                    dtpickCounter++;
                    Block_time++;
                    $("#Block_time").val(Block_time);
                }
                else
                {
                    alert("Max image limit 10 ");
                }
            });

            $("body").on("click",".remove",function(){
                var Block_time = $("#Block_time").val();
                Block_time--;
                $("#Block_time").val(Block_time);
                $(this).parent(".colM").remove();
            });
        });
        function dltImg(id){
            if(confirm('Are You Sure?')) {
                $.ajax({
                    type: "POST",
                    url: '{{url('Admin/dltimage')}}',
                    data: {
                        id: id
                    },
                    success: function (data) {
                        $("#img"+id).remove();
                    },
                    error: function (data) {

                    }
                })
            }else{
                return false;
            }
        }
    </script>

    <script>

    </script>
@endsection
