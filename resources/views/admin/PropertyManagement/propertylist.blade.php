@extends('admin.layouts.app')

@section('pageTitle', 'Dashboard')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Property List
                <small>List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Property List</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">All Property List</h3>
                        </div><!-- /.box-header -->

                        @if(Auth::user()->type == 2)
                            <div class="sortpr pull-right">
                                <label>Sort By Owner</label>
                                <form action="{!! url('Admin/propertylist') !!}" method="get" class="sortingForm">
                                    <select name="owner" onchange="$('.sortingForm').submit()">
                                        <option value="" @if($usrid == '') selected @endif>All Owner</option>
                                        @foreach($user as $key)
                                            <option value="{{$key['id']}}" @if($usrid == $key['id']) selected @endif>{{$key['first_name']}} {{$key['last_name']}}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </div>
                        @endif
                        {{--<div class="headerSearchBox pull-right">--}}
                            {{--<form action="{!! admin_url('product-list') !!}" method="get">--}}
                                {{--<input type="text" placeholder="Search Product" class="field" name="keyword" value="{{ request()->input('keyword') }}" required="" id="search">--}}
                                {{--<input type="hidden" name="sort_by" value="{{ request()->input('sort_by') }}">--}}
                                {{--<button class="button pink" type="submit" id="searchList"><span>SEARCH</span></button>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                        <div class="box-body">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <p>{!! $error !!}</p>
                                    @endforeach
                                </div>
                            @endif
                            @if(session('success'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {!! session('success') !!}
                                </div>
                            @endif
                            <table id="list_table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Property Name</th>
                                    <th> Rent</th>
                                    <th> Location </th>
                                    <th> Deposit </th>
                                    <th> Room Size </th>
                                    <th> Bedrooms </th>
                                    <th> Bath Rooms </th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($properties as $property)

                                    <tr>
                                        <td>{{ $property->Property_title }}</td>
                                        <td>@if($property->Rent_currency_type == 'US$' || $property->Rent_currency_type == 'HK$')$@else{{$property->Rent_currency_type}}@endif{{ $property->Rent }}</td>
                                        <td>{{ $property->Location }}</td>
                                        <td>{{ $property->Deposit }}</td>
                                        <td>{{ $property->Room_size }}</td>
                                        <td>{{ $property->Bedrooms }}</td>
                                        <td>{{ $property->Bathroom }}</td>
                                        <td>
                                            <a href="{{url('Admin/editproperty/'.$property->id)}}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                            <a href="{{url('Admin/dltProperty/'.$property->id)}}" onclick="return confirm('Are you sure?')" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true" ></i> Delete</a>
                                            <a href="{{url('Admin/fetchblckdt/'.$property->id)}}" class="btn btn-sm btn-warning td-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Blocked Dates</a>
                                        </td>
                                    </tr>

                                @endforeach



                                </tbody>
                            </table>
                            {{--{{ $products->appends($_GET)->render() }}--}}
                            {{ $properties->appends(request()->query())->links() }}
                        </div><!-- /.box-body -->
                        <div class="paginationDiv">

                        </div>
                    </div><!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->

@endsection