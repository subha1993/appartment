<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block_count extends Model
{
    protected $fillable = ['user_id', 'property_id','from_datetime','to_datetime'];
}
