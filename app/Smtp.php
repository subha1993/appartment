<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Smtp extends Model
{
    protected $fillable = [
        'user_id', 'mail_host', 'mail_port', 'mail_username', 'mail_password','mail_encryption'
    ];
}
