<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emailtemplate extends Model
{
     protected $table = 'email_template';
    protected $fillable = [
        'user_id', 'emailTitle', 'description', 'type'
    ];
}
