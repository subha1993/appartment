<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class addmanagermail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var User
     */
    public $user;

    /**
     * The reset password link
     *
     * @var string
     */
    public $confirm_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( User $user, $password)
    {
        //
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Manager Invitation')->markdown('admin.email.addmanagermail', ['user' => $this->user, 'password' => $this->password]);
    }
}
