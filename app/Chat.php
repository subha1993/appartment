<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
   protected $fillable = [
        'id', 'manager', 'user_device', 'apartment', 'status'
    ];

    public function propertydetails(){
        return $this->belongsTo('App\PropertyManagement', 'apartment', 'id');
    }

    public function unreadmsg(){
        return $this->hasMany('App\Message', 'chatId', 'id');
    }
}
