<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'chatId', 'propertyId', 'message', 'created_at'
    ];

    public function propertyDetails(){
        return $this->belongsTo('App\PropertyManagement', 'propertyId', 'id');
    }
}
