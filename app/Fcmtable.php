<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fcmtable extends Model
{
    protected $table = 'fcmtable';
    protected $fillable = [
        'user', 'fcmtoken'
    ];
}
