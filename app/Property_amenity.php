<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property_amenity extends Model
{
    protected $fillable = ['user_id', 'property_id', 'amenity_id','amenity_root_id'];
    
    public function amenities()
    {
        return $this->hasOne('App\Amenity', 'id', 'amenity_id');
    }
}
