<?php

namespace App\Http\Controllers\Admin;
use App\User;
use App\Country;
use App\Neighborhood;
use App\Amenity;
use App\Image;
use App\PropertyManagement;
use App\Property_amenity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Block_count;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Img;

class addPropertyController extends Controller
{
  public function __construct()
  {
      $this->middleware('adminMiddleware');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function AddProperty(Request $request)
    {
            set_time_limit(0);
            // dd($request->file('add_images'));
          //dd($request->all());

            $rules = [
                'Property_Title' => 'required|string',
                'Property_Description' => 'required',
                'Location' => 'required',
                //'City'=>'required',
                // 'borough'=>'required',
                'neighborhood' => 'required',
                'Address1' => 'required',
                'Rent' => 'required',
                //  'Room_size'=>'required',
                'add_images' => 'required',
            ];

            $customMessages = [
                'required' => 'The :attribute field can not be blank.'
            ];

            $this->validate($request, $rules, $customMessages);

//$this->validate($request,
//      ['Property_Title' => 'required|string',
//      'Property_Description'=>'required',
//      'Location' => 'required',
//      'City'=>'required',
//      'Country'=>'required',
//      'Address1'=>'required',
//      'Rent' => 'required',
//      'Room_size'=>'required'],
//	  ['add_images.required'=>'Please add at least one image',]
//);

            $bsstp = '';
            $data = $request;
        $a = $data['Rent'];
        if(strpos($a, ',') !== false) {
            $a = str_replace(',', '', $a);
        }
        if(strpos($a, '$') !== false) {
            $data['Rent'] = ltrim($a, '$');
        }else{
            $data['Rent']=$a;
        }

//        $url="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=".$data['addr_lat'].",".$data['addr_long']."&rankby=distance&type=bus_station&key=AIzaSyA89fgLxCORT7hFgqF9uWX0MUzGadIpF84";
//
//        $options = array(
//            CURLOPT_RETURNTRANSFER => true,   // return web page
//            CURLOPT_HEADER         => false,  // don't return headers
//            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
//            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
//            CURLOPT_ENCODING       => "",     // handle compressed
//            CURLOPT_USERAGENT      => "test", // name of client
//            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
//            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
//            CURLOPT_TIMEOUT        => 120,    // time-out on response
//        );
//
//        $ch = curl_init($url);
//        curl_setopt_array($ch, $options);
//
//        $content  = curl_exec($ch);
//
//        curl_close($ch);
//
//        //return $content;
//        $resArr = json_decode($content);
//
//        if(!empty($resArr->results)) {
//            $nbsstp = $resArr->results[0];
//            $bsstp=$nbsstp->name;
//        }
            // echo "<pre>"; print_r($bsstp->name); echo "</pre>";

            $PropertyManagement = PropertyManagement::create([

                //'user_id' => Auth::user()->id,
                'user_id' => $data['owner'],
                'token' => str_random(50),
                'Property_title' => $data['Property_Title'],
                'Property_Description' => $data['Property_Description'],
                //'price_currency_type' => $data['price_currency_type'],
                //'price' => $data['price'],
                'Bedrooms' => $data['Number_of_Bedrooms'],
                'Bathroom' => $data['Number_of_Bathroom'],
                'Location' => $data['Location'],
                'loc_lat' => $data['addr_lat'],
                'loc_long' => $data['addr_long'],
                'city' => '',
                //  'country' => $data['Country'],
                'borough' => 0,
                'neighborhood' => $data['neighborhood'],
                'Full_Address1' => $data['Address1'],
                'Full_Address2' => '',
                'display_address' => $data['Address1'],
                'zipcode' => '',
                'Rent_currency_type' => 'US$',
                'Rent' => $data['Rent'],
                'unit_rent' => 'per month',
                'Bills' => '',
                'Deposit' => '',
                'Room_size' => '',
                'Block_time_count' => $data['Block_time'],
                'nearest_busstop' => $bsstp,
                //'imagecount' => $data['imagecount'],
            ]);
            //dd($PropertyManagement['id']);
            /*if(Input::hasFile('image')){
                 $file=Input::file('image');
                 $fileName = str_random(60);
                 $file->move(public_path().'/assets/admin/PropertyManagement/image',$fileName);
                 Image::create([
                 'user_id' => Auth::user()->id,
                 'property_id' => $PropertyManagement['id'],
                 'image_name' => $fileName,
               ]);
              }*/
            if ($request->file('add_images')) {
                //dd($request->file('add_images'));
                foreach ($request->file('add_images') as $image) {
                    $fileName = str_random(60);
                    $file = $image;
                    $name = time() . rand(100, 999) . mt_rand(100, 999) . str_random(10) . '.' . $file->getClientOriginalExtension();

                    $filePathAWS = 'admin/PropertyManagement/image/' . $name;
                    $cmprsdPathAWS = 'admin/PropertyManagement/cmprsdimage/' . $name;
                    //dd($file);
                    try {
                        Storage::disk('s3')->put($filePathAWS, file_get_contents($file), 'public');
                        // $image->move(public_path().'/assets/admin/PropertyManagement/image',$fileName.'.'.$image->getClientOriginalExtension());
                        $img = Img::make(env('AWS_BUCKET_URL').'admin/PropertyManagement/image/'.$name)->resize(400, 300)->stream();
                        Storage::disk('s3')->put($cmprsdPathAWS, $img->__toString(),'public');
                        Image::create([
                            'user_id' => Auth::user()->id,
                            'property_id' => $PropertyManagement['id'],
                            'image_name' => $name,
                        ]);
                    } catch (Exception $e) {
                        dd($e->getMessage());
                    }
                }

            }
            if ($request['amenities'] != null) {
                //dd($request['amenities']);
                foreach ($request['amenities'] as $amenity) {
                    foreach ($amenity as $subamenity) {
                        $Property_amenity = Property_amenity::create([
                            'user_id' => Auth::user()->id,
                            'property_id' => $PropertyManagement['id'],
                            'amenity_id' => $subamenity,
                        ]);
                    }
                }
            }
//        if($request['from_datetime']!=null){
//          //dd($request['from_datetime']);
//           /*foreach(array_combine($request['from_datetime'], $request['to_datetime']) as $datetimestart =>$datetimeend)
//           {
//             //echo "From".$datetimestart."</br>To".$datetimeend;
//             //dd($datetimestart);
//                $bcount=Block_count::create([
//                'user_id' => Auth::user()->id,
//                'property_id' => $PropertyManagement['id'],
//                'from_datetime'=>$datetimestart,
//                'to_datetime'=>$datetimeend,
//              ]);
//              //dd($bcount);
//           }*/
//		   foreach($request['from_datetime'] as $datetimestart){
//            $count=1;
////             foreach(explode("-",$datetimestart) as $datetime){
////               if($count==1){
////                   $time = strtotime($datetime);
////                   $datetime = date('Y-m-d',$time);
////                 $start=$datetime;
////                 $count++;
////               }else{
////                   $time = strtotime($datetime);
////                   $datetime = date('Y-m-d',$time);
////                 $end=$datetime;
////               }
////             }
//               $datetime = explode("-",$datetimestart);
////                str_replace(find,replace,string,count)
//               $time = strtotime(trim($datetime[0]));
//               $dtm = date('Y-m-d',$time);
//               $start=$dtm;
//               //dd();
//               $endtm=$datetime[1];
//               $tm = strtotime(trim($endtm));
//               $dtm = date('Y-m-d',$tm);
//               $end=$dtm;
//             $bcount=Block_count::create([
//             'user_id' => Auth::user()->id,
//             'property_id' => $PropertyManagement['id'],
//             'from_datetime'=>$start,
//             'to_datetime'=>$end,
//           ]);
//           }
//        }

            return redirect()->back()->withSuccess("Property Added");

}

    public function propertylist(){
//        if(Auth::user()->type == 2) {
//            $properties = PropertyManagement::orderBy('id','DESC')->paginate(15);
//        }else if(Auth::user()->type == 3){
//            $properties = PropertyManagement::where('user_id',Auth::id())->orderBy('id','DESC')->paginate(15);
//        }
//        $data['properties']=$properties;
//        return view('admin.PropertyManagement.propertylist',$data);
        $usr = Input::get('owner') ?  Input::get('owner') : '';
        if(Auth::user()->type == 2) {
            if($usr == '') {
                $properties = PropertyManagement::orderBy('id','DESC')->paginate(15);
            }else{
                $properties = PropertyManagement::where('user_id',$usr)->orderBy('id','DESC')->paginate(15);
            }
        }else if(Auth::user()->type == 3){
            $properties = PropertyManagement::where('user_id',Auth::id())->orderBy('id','DESC')->paginate(15);
        }
        $user=User::with('properties')->get()->toArray();
        foreach($user as $key => $value){
            if(empty($value['properties'])){
                unset($user[$key]);
            }
        }
        $usrid='';
        if($usr != ''){
            $usrid=$usr;
        }
        $data['usrid']=$usrid;
        $data['properties']=$properties;
        $data['user']=$user;
        return view('admin.PropertyManagement.propertylist',$data);
    }

    public function propertyedit($id)
    {
        $countries = Country::all();
        $amenities = Amenity::all();
        $users = User::all();
        $borrogh = Neighborhood::where('parent_id',0)->get();
        $data=[
            'countries' => $countries,
            'amenities' => $amenities,
            'borrogh' => $borrogh,
            'users' => $users
        ];
        $data['property']=PropertyManagement::where('id',$id)->with('images','propertyamenity','blockcount','propertyuser')->first();
        //dd($data['property']);
        $data['neighborhood'] = Neighborhood::where('parent_id',1)->get();
        //$property=PropertyManagement::where('id',$id)->with('images','propertyamenity','blockcount')->first()->toArray();
        //dd($property['blockcount'][0]);
        //dd($data);

        return view('admin.PropertyManagement.editproperty',$data);
    }

    public function dltpropertyimage(Request $request){
        $id=$request->has('id') ? $request->input('id') : '';
        Image::where('id', $id)->delete();
        $data['response']='success';
        return response()->json($data, 200);
    }

    public function updtProperty(Request $request){
        //dd($request->all());
        $this->validate($request, [
            'Property_Title' => 'required|string',
            'Property_Description'=>'required',
            'Location' => 'required',
          //  'City'=>'required',
           // 'Country'=>'required',
            //'borough'=>'required',
            'neighborhood'=>'required',
            'Address1'=>'required',
            'Rent' => 'required',
            //'Room_size'=>'required'
        ]);
        //$bsstp='';
        $data=$request;
        $a = $data['Rent'];
        if(strpos($a, ',') !== false) {
            $a = str_replace(',', '', $a);
        }
        if(strpos($a, '$') !== false) {
            $data['Rent'] = ltrim($a, '$');
        }else{
            $data['Rent']=$a;
        }
//        $url="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=".$data['addr_lat'].",".$data['addr_long']."&rankby=distance&type=bus_station&key=AIzaSyDMdePm-gNIYik5mrTG6w8loB-phE_aHX4";
//        $options = array(
//            CURLOPT_RETURNTRANSFER => true,   // return web page
//            CURLOPT_HEADER         => false,  // don't return headers
//            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
//            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
//            CURLOPT_ENCODING       => "",     // handle compressed
//            CURLOPT_USERAGENT      => "test", // name of client
//            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
//            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
//            CURLOPT_TIMEOUT        => 120,    // time-out on response
//        );
//
//        $ch = curl_init($url);
//        curl_setopt_array($ch, $options);
//
//        $content  = curl_exec($ch);
//
//        curl_close($ch);
//
//        //return $content;
//        $resArr = json_decode($content);
//        echo "<pre>"; print_r($resArr); echo "</pre>";die();
//        if(!empty($resArr->results)) {
//            $nbsstp = $resArr->results[0];
//            $bsstp=$nbsstp->name;
//        }

        $id=$data['prid'];
        $prmgmt = PropertyManagement::find($id);
        $prmgmt->Property_title=$data['Property_Title'];
        $prmgmt->Property_Description=$data['Property_Description'];
        $prmgmt->user_id = $data['owner'];
//        $prmgmt->price_currency_type=$data['price_currency_type'];
//        $prmgmt->price=$data['price'];
        $prmgmt->Bedrooms=$data['Number_of_Bedrooms'];
        $prmgmt->Bathroom=$data['Number_of_Bathroom'];
        $prmgmt->Location=$data['Location'];
        $prmgmt->loc_lat=$data['addr_lat'];
        $prmgmt->loc_long=$data['addr_long'];
        //$prmgmt->country=$data['Country'];
      //  $prmgmt->borough=$data['borough'];
        $prmgmt->neighborhood=$data['neighborhood'];
        $prmgmt->city='';
        $prmgmt->Full_Address1=$data['Address1'];
        $prmgmt->Full_Address2='';
        $prmgmt->display_address=$data['Address1'];
        $prmgmt->zipcode='';
        $prmgmt->Rent_currency_type='US$';
        $prmgmt->Rent=$data['Rent'];
        $prmgmt->unit_rent='per month';
        $prmgmt->Bills='';
        $prmgmt->Deposit='';
        $prmgmt->Room_size='';
        $prmgmt->Block_time_count=$data['Block_time'];
        //$prmgmt->nearest_busstop=$bsstp;

        $prmgmt->save();
        if(Input::hasFile('add_images')){
            //dd($request->file('add_images'));
            foreach($request->file('add_images') as $image)
            {
                $fileName = str_random(60);
                $file = $image;
                $name = time().rand(100, 999).mt_rand(100, 999).str_random(10).'.'.$file->getClientOriginalExtension();

                $filePathAWS = 'admin/PropertyManagement/image/' . $name;
                $cmprsdPathAWS = 'admin/PropertyManagement/cmprsdimage/' . $name;
                try {
                    Storage::disk('s3')->put($filePathAWS, file_get_contents($file), 'public');
                    $img = Img::make(env('AWS_BUCKET_URL').'admin/PropertyManagement/image/'.$name)->resize(400, 300)->stream();
                    Storage::disk('s3')->put($cmprsdPathAWS, $img->__toString(),'public');
              //  $image->move(public_path().'/assets/admin/PropertyManagement/image',$fileName.'.'.$image->getClientOriginalExtension());
                Image::create([
                    'user_id' => Auth::user()->id,
                    'property_id' => $id,
                    'image_name' => $name,
                ]);
                }
                catch (Expection $e){
                    dd($e->getMessage());
                }


            }
        }

        Property_amenity::where('property_id',$id)->delete();

        if($request['amenities']!=null){
            //dd($request['amenities']);
            foreach($request['amenities'] as $amenity){
                foreach($amenity as $subamenity){
                    $Property_amenity=Property_amenity::create([
                        'user_id' => Auth::user()->id,
                        'property_id' => $id,
                        'amenity_id' => $subamenity,
                    ]);
                }
            }
        }

       // Block_count::where('property_id',$id)->delete();

//        if($request['from_datetime']!=null){
//            //dd($request['from_datetime']);
//            /*foreach(array_combine($request['from_datetime'], $request['to_datetime']) as $datetimestart =>$datetimeend)
//            {
//              //echo "From".$datetimestart."</br>To".$datetimeend;
//              //dd($datetimestart);
//                 $bcount=Block_count::create([
//                 'user_id' => Auth::user()->id,
//                 'property_id' => $PropertyManagement['id'],
//                 'from_datetime'=>$datetimestart,
//                 'to_datetime'=>$datetimeend,
//               ]);
//               //dd($bcount);
//            }*/
//            foreach($request['from_datetime'] as $datetimestart){
//                $count=1;
////                foreach(explode("-",$datetimestart) as $datetime){
////                    if($count==1){
////                        $time = strtotime($datetime);
////                        $dtm = date('Y-m-d',$time);
////                        $start=$dtm;
////                        $count++;
////                    }else{
////                        $tm = strtotime($datetime);
////                        $dtm = date('Y-m-d',$tm);
////                        $end=$dtm;
////                    }
////                }
//               $datetime = explode("-",$datetimestart);
////                str_replace(find,replace,string,count)
//                $time = strtotime(trim($datetime[0]));
//                $dtm = date('Y-m-d',$time);
//                $start=$dtm;
//                //dd();
//                $endtm=$datetime[1];
//                $tm = strtotime(trim($endtm));
//                $dtm = date('Y-m-d',$tm);
//                $end=$dtm;
//                $bcount=Block_count::create([
//                    'user_id' => Auth::user()->id,
//                    'property_id' => $id,
//                    'from_datetime'=>$start,
//                    'to_datetime'=>$end,
//                ]);
//            }
//        }


        return redirect()->back()->withSuccess("Property Updated");


    }

    public function dltProperty($id){
        PropertyManagement::where('id',$id)->delete();
        Image::where('property_id',$id)->delete();
        Property_amenity::where('property_id',$id)->delete();
        Block_count::where('property_id',$id)->delete();
        return redirect()->back()->withSuccess("Property Deleted");
    }

    public function blockdate($id){
//        if(Auth::user()->type == 2){
//            $property=PropertyManagement::all();
//        }elseif (Auth::user()->type == 3){
            $property=PropertyManagement::find($id);
//        }
        //dd($property);
        $data['property'] = $property;
        return view('admin.PropertyManagement.propertyBlock',$data);
    }

    public function addblockdate(Request $request){
        $prpt=$request['property'];
        		   foreach($request['from_datetime'] as $datetimestart){
            $count=1;
//             foreach(explode("-",$datetimestart) as $datetime){
//               if($count==1){
//                   $time = strtotime($datetime);
//                   $datetime = date('Y-m-d',$time);
//                 $start=$datetime;
//                 $count++;
//               }else{
//                   $time = strtotime($datetime);
//                   $datetime = date('Y-m-d',$time);
//                 $end=$datetime;
//               }
//             }
               $datetime = explode(" - ",$datetimestart);
//                str_replace(find,replace,string,count)
               $time = strtotime(trim($datetime[0]));
               $dtm = date('Y-m-d H:i:s',$time);
               $start=$dtm;
               //dd();
               $endtm=$datetime[1];
               $tm = strtotime(trim($endtm));
               $dtm = date('Y-m-d H:i:s',$tm);
               $end=$dtm;
             $bcount=Block_count::create([
             'user_id' => Auth::id(),
             'property_id' => $prpt,
             'from_datetime'=>$start,
             'to_datetime'=>$end,
           ]);
           }
       //return redirect()->back()->withSuccess("Dates Added");
        return redirect(url('Admin/fetchblckdt/'.$prpt))->withSuccess("Dates Added Successfully");

    }

    public function getBlockDates(){
        if(Auth::user()->type == 2){
            $property=PropertyManagement::all();
        }elseif (Auth::user()->type == 3){
            $property=PropertyManagement::where('user_id',Auth::id())->get();
        }
        //dd($property);
        $data['property'] = $property;
        return view('admin.PropertyManagement.blockdatelist',$data);
    }

    public function blockdatelist($id){
        $data['blck']=Block_count::where('property_id',$id)->get();
        $data['prprt']=PropertyManagement::find($id);
       // dd($data['blck']);
        return view('admin.PropertyManagement.blockdatelist',$data);
    }

    public function deleteblckdt(Request $request){
        $id=$request['id'];
        Block_count::where('id',$id)->delete();
        $data['response'] = 'success';
        return response()->json($data, 200);
    }

    public function getscrapdata(){
        //$url='https://streeteasy.com/for-sale/manhattan/price:100000-30000000?page=2';
        $url='https://streeteasy.com/building/the-club-at-turtle-bay/ph2c';
//        $ch = curl_init();
//        $timeout = 5;
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
//        $data = curl_exec($ch);
//        curl_close($ch);
//        print_r($data);die();
//file_get_contents() reads remote webpage content
        $lines_string=file_get_contents($url);
//output, you can also save it locally on the server
        echo '<pre>';
        echo htmlspecialchars($lines_string);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

