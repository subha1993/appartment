<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
      Auth::logout();
      session()->flush();
      return redirect('/admin');
    }


    public function showLoginForm()
    {
        return view('admin.auth.login');
    }
    public function login(Request $request)
    {

      $this->validate($request, [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string',
        ]);
      $admin=User::where(['email' => $request['email'],'type' => 2])->orWhere(function($query)use($request){
       $query->where(['email' => $request['email'],'type' => 3]);
      })->first();
      if (isset($admin)){
        // if (Hash::check($request['password'],$admin['password'])){
			if($admin->is_active=='Y'){
				if (Auth::attempt(['email' => $request['email'],'password' =>$request['password'],'type' => 2]) || Auth::attempt(['email' => $request['email'],'password' =>$request['password'],'type' => 3])){
            return redirect('Admin/DashBoard');
            }else{
                return redirect()->back()->withErrors('Your Password is Incorrect');
            }
			}else{
				if($admin->is_active=='B'){
					return redirect()->back()->withErrors('Your Account is Blocked');
				}else{
					return redirect()->back()->withErrors('Your Account is not Active Yet');
				}
			}
      }else{
        return redirect()->back()->withErrors('Incorrect Email Id');
      }
    }
}
