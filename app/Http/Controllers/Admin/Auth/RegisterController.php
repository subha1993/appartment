<?php

namespace App\Http\Controllers\Admin\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\verifyEmail;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/Admin/DashBoard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function showRegistrationForm()
    {
//        retriveLang();
//        $countries = Country::all();
//        $data = [
//            'countries' => $countries,
//
//        ];
        return view('admin.auth.register');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      Session::flash('status','registared! but verify your email to active account');
        return User::create([
			'verifyToken'=>Str::random(60),
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'type' => '3',
        ]);
    }
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        $thisUser=User::findOrfail($user->id);
        //dd($thisUser);
        $this->sendEmail($thisUser);
        return redirect()->back()->with('success', 'Registered successfully but verify your Email to active account');
        //return $user;
    }

    public function sendEmail($thisUser){
		  //dd($thisUser);
      Mail::to($thisUser['email'])->send(new verifyEmail($thisUser));
    }

    public function sendEmailDone($email,$verifyToken){
      $user=User::where(['email'=>$email,'verifyToken'=>$verifyToken])->first();
      if (isset($user)){
        $active=User::where(['email'=>$email,'verifyToken'=>$verifyToken])->update(['is_active'=>'Y','verifyToken'=>NULL]);
        //dd($active);
        return redirect('admin')->with('success','Your Account is Active Now');
      }else{
        return 'user not found';
      }
    }
}
