<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\addmanagermail;
use App\User;
use Auth;
class Usermanagement extends Controller
{
  public function __construct()
  {
      $this->middleware('adminMiddleware');
  }

  public function addManager(){
      return view('admin.usermanagement.addManager');
  }

    public function userlist(){
      $allUsers=User::where('type',3)->orderBy('id','DESC')->paginate(10);
      $data['allUsers']=$allUsers;
	  if(Auth::user()->type == 2){
		  return view('admin.usermanagement.usermanagement',$data);
	  }else{
		  return redirect('admin/logout');
	  }
    }
	
	public function userdelete(Request $request){
      $data=User::where(['id'=>$request['userid']])->first();
      if (isset($data)){
        User::where('id',$data['id'])->delete();
      }
      return redirect()->back();
    }
	
	public function userblock(Request $request){
      $data=User::where(['id'=>$request['userid']])->first();
      if (isset($data)){
        User::where('id',$data['id'])->update(['is_active'=>'B']);
      }
      return redirect()->back();
    }

    public function useractive(Request $request){
      $data=User::where(['id'=>$request['userid']])->first();
      if (isset($data)){
        User::where('id',$data['id'])->update(['is_active'=>'Y']);
      }
      return redirect()->back();
    }
	
	    public function useredit(Request $request){
      $data=User::where(['id'=>$request['userid']])->first();
      if (isset($data)){
        $data=[
          'userdatas'=>User::where('id',$data['id'])->get(),
        ];
        return view('admin.usermanagement.editManager',$data);
      }
    }
	
	    public function editManager(Request $request){
      $this->validate($request, [
            'first_name' => 'required|string',
            'last_name'=>'required',
            'email' => 'required|string',
            'phone'=>'required',
        ]);
       $data=User::where(['id'=>$request['userid']])->first();
       if (isset($data)){
		   
		   if($request['password']!=null){
           User::where('id',$request['userid'])->update([
             'password'=> bcrypt($request['password']),
           ]);
         }
		   
         User::where('id',$request['userid'])->update([
           'first_name'=>$request['first_name'],
           'last_name'=>$request['last_name'],
           'email'=>$request['email'],
           'phone_number'=>$request['phone'],
         ]);
        return redirect()->back()->withSuccess("Update Successfull");
       }
    }

    public function addManagerProcess(Request $request){
       // dd($request['status']);
        $data=$request->all();
        $this->validate($request, [
            'firstName' => 'required|string',
            'lastName' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'phone_number' => 'required|numeric|unique:users',
            'password' => 'required|string|min:6'
        ]);
        $st='';
        if($data['status'] == 'Y'){
            $st='Y';
        }else{
            $st='N';
        }
        $usr=User::create([
            'first_name' => $data['firstName'],
            'last_name' => $data['lastName'],
            'email' => $data['email'],
            'phone_number' => $data['phone_number'],
            'is_active' => $st,
            'password' => bcrypt($data['password']),
            'type' => '3'
        ]);

        Mail::to($data['email'])->send(new addmanagermail($usr, $data['password']));

        return redirect()->back()->withSuccess("User Added Successfully");
    }
	
}
