<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Country;
use App\Amenity;
use App\Neighborhood;
use App\Pricerange;
use App\Contact;
use App\Smtp;
use App\Emailtemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Illuminate\Support\Facades\Mail;
use App\Mail\managermail;
use Auth;
use Config;
use App;
class HomeController extends Controller
{
      /**
       * Create a new controller instance.
       *
       * @return void
       */
      public function __construct()
      {
          $this->middleware('adminMiddleware');
      }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
		 //dd("Hello");
         //return view('Admin.auth.dashboard');
         return view('admin.dashboard');
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function AddProperty()
    {


      $countries = Country::all();
      $amenities = Amenity::all();
      $borrough = Neighborhood::where('parent_id',0)->get();
      $neighborhood = Neighborhood::where('parent_id','=',1)->get();
      $users = User::all();
      $data=[
        'countries' => $countries,
        'amenities' => $amenities,
        'borrogh' => $borrough,
        'neighborhood' => $neighborhood,
        'users' => $users
      ];
      return view('admin.PropertyManagement.addProperty',$data);
    }

    public function addamenity(){
        if(Auth::user()->type == 2) {
            return view('admin.amenity.addamenity');
        }else{
            return view('admin.dashboard');
        }
    }
    public function addlaundry(){
        if(Auth::user()->type == 2) {
        return view('admin.amenity.addlaundry');
        }else{
            return view('admin.dashboard');
        }
    }
    public function addpetamenity(){
        if(Auth::user()->type == 2) {
        return view('admin.amenity.addpetamenity');
        }else{
            return view('admin.dashboard');
        }
    }

    public function addamenityprocess(Request $request){
            // dd($request->toArray());
            $this->validate($request, [
                'name' => 'required|string',
                'logo' => 'required',
            ]);
            $data = $request->all();
            if (Input::hasFile('logo')) {
                $file = Input::file('logo');
                $name = time() . rand(100, 999) . mt_rand(100, 999) . str_random(10) . '.' . $file->getClientOriginalExtension();

                $filePathAWS = 'admin/amenitylogo/' . $name;
                Storage::disk('s3')->put($filePathAWS, file_get_contents($file), 'public');

//            $file=Input::file('logo');
//            $fileName = str_random(60);
//            $file->move(public_path().'/assets/admin/amenitylogo',$fileName.'.'.$file->getClientOriginalExtension());
                Amenity::create([
                    'root_id' => $data['root'],
                    'fields' => $data['name'],
                    'logo' => $name,
                ]);
            }
            return redirect()->back()->withSuccess("Aminity Added");

    }

    public function amenitylist(){
        if(Auth::user()->type == 2) {
            $amenities = Amenity::where('root_id', 1)->get();
            $data = [
                'amenities' => $amenities,
            ];
            return view('admin.amenity.amenities', $data);
        }else{
            return view('admin.dashboard');
        }
    }

    public function laundrylist(){
        if(Auth::user()->type == 2) {
        $amenities = Amenity::where('root_id',2)->get();
        $data=[
            'amenities' => $amenities,
        ];
        return view('admin.amenity.laundrylist',$data);
        }else{
            return view('admin.dashboard');
        }
    }
    public function petamenitylist(){
        if(Auth::user()->type == 2) {
        $amenities = Amenity::where('root_id',3)->get();
        $data=[
            'amenities' => $amenities,
        ];
        return view('admin.amenity.pet_amenities',$data);
        }else{
            return view('admin.dashboard');
        }
    }

    public function editamenity($id){
        $data['amenity']=Amenity::find($id);
        return view('admin.amenity.editamenity',$data);
    }

    public function updtamenity(Request $request){
        $data=$request;
        $id=$data['amid'];
        $amnt = Amenity::find($id);
        $fileName='';
        if(Input::hasFile('logo')) {

            $file = Input::file('logo');
            $name = time().rand(100, 999).mt_rand(100, 999).str_random(10).'.'.$file->getClientOriginalExtension();

            $filePathAWS = 'admin/amenitylogo/' . $name;
            Storage::disk('s3')->put($filePathAWS, file_get_contents($file), 'public');

//            $file = Input::file('logo');
//            $fileName = str_random(60);
//            $file->move(public_path() . '/assets/admin/amenitylogo', $fileName.'.'.$file->getClientOriginalExtension());
        }
        $amnt->fields = $data['name'];
        if($fileName != ''){
            $amnt->logo = $fileName.'.'.$file->getClientOriginalExtension();
        }
        $amnt->save();
        return redirect()->back()->withSuccess("Aminity Updated");



    }

    public function dltamenity($id){
        Amenity::where('id',$id)->delete();
        return redirect()->back()->withSuccess("Aminity Deleted");
    }

    public function loadneighbor(Request $request){
        $id=$request['id'];
        $data['neighborhood']=Neighborhood::where('parent_id',$id)->get();
        return view('admin.PropertyManagement.loadneighborhood',$data);
    }

    public function addPriceRange(){
        return view('admin.custompricerange');
    }

    public function addPriceRangeProcess(Request $request){
     //   $sts = $request['status'];
//        if($sts == 'n'){
            $this->validate($request, [
                'minprice' => 'required|numeric',
                'maxprice' => 'required|numeric',
            ]);
//        }
        $data = $request;
        Pricerange::create([
            'minprice' => $data['minprice'],
            'maxprice' => $data['maxprice'],
            'status' => $data['status']
        ]);

        return redirect()->back()->withSuccess("Price Range Added");


    }

    public function pricerangelist(){
        $data['pricerngs'] = Pricerange::all();
        return view('admin.pricerangelist',$data);
    }

    public function deletepricerange($id){
        Pricerange::where('id',$id)->delete();
        return redirect()->back()->withSuccess("Price Range Deleted");
    }

    public function contactform(){
        return view('admin.addcontact');
    }

    public function addContact(Request $request){
//        $this->validate($request, [
//            'email' => 'required|string|email||unique:contacts'
//        ]);
        Contact::create([
            'user_id' => Auth::id(),
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'status' => $request['status'],
        ]);
        return redirect()->back()->withSuccess("Contact Added");
    }

    public function contactlist(){
        $data['contact']=Contact::where('user_id',Auth::id())->get();
        $data['emailtmplts'] = Emailtemplate::where('user_id',Auth::id())->get();
        return view('admin.contactlist',$data);
    }

    public function editContact($id){
       $data['contact']=Contact::find($id);
       return view('admin.editcontact',$data);
    }

    public function updtContact(Request $request){
        $contact=Contact::find($request['cid']);
        $contact->name=$request['name'];
        $contact->email=$request['email'];
        $contact->phone=$request['phone'];
        $contact->status=$request['status'];
        $contact->save();
        return redirect()->back()->withSuccess("Contact Updated");
    }

    public function dltContact($id){
        Contact::where('id',$id)->delete();
        return redirect()->back()->withSuccess("Contact Deleted");
    }

    public function emailtemplate(){
        return view('admin.email_template');
    }

    public function addemailtemplate(Request $request){
        Emailtemplate::create([
            'user_id'=>Auth::id(),
            'emailTitle'=>$request['email_Title'],
            'description'=>$request['email_description'],
            'type'=>'managermail'
        ]);
        return redirect(url('Admin/addemailtemp'))->withSuccess("Email Added");
    }

    public function emaillist(){
        $data['emails']=Emailtemplate::where('user_id',Auth::id())->get();
        return view('admin.emaillist',$data);
    }

    public function editEmail($id){
        $data['email']=Emailtemplate::find($id);
        return view('admin.editemail',$data);
    }

    public function updtEmail(Request $request){
        $id=$request['eid'];
        $emltmplt=Emailtemplate::find($id);
        $emltmplt->emailTitle=$request['email_Title'];
        $emltmplt->description=$request['email_description'];
        $emltmplt->save();
        return redirect(url('Admin/editemail/'.$id))->withSuccess("Email Updated");
    }

    public function dltEmail($id){
        Emailtemplate::where('id',$id)->delete();
        return redirect(url('Admin/emaillist'))->withSuccess("Email Deleted");
    }

    public function smtp(){
        $data['smtp']=Smtp::where('user_id',Auth::id())->first();
        return view('admin.addsmtp',$data);
    }

    public function addsmtp(Request $request){

     $smtp=Smtp::where('user_id',Auth::id())->first();
    // dd($smtp);
     if(!empty($smtp)){
         $smtp->mail_host=trim($request['host']);
         $smtp->mail_port=trim($request['port']);
         $smtp->mail_username=trim($request['username']);
         $smtp->mail_password=trim($request['password']);
         $smtp->mail_encryption=trim($request['encryption']);
         $smtp->save();
         return redirect(url('Admin/smtp'))->withSuccess("Smtp Updated");
     }else{
         Smtp::create([
            'user_id'=>Auth::id(),
            'mail_host'=>trim($request['host']),
            'mail_port'=>trim($request['port']),
            'mail_username'=>trim($request['username']),
            'mail_password'=>trim($request['password']),
            'mail_encryption'=>trim($request['encryption']),
         ]);
         return redirect(url('Admin/smtp'))->withSuccess("Smtp Added");
     }
    }

    public function sendmanagermail(Request $request){
        $flg=0;
        $mailcnfg=Smtp::where('user_id',Auth::id())->first();
        if(!empty($mailcnfg)) {
            $config = array(
                'driver' => 'smtp',
                'host' => $mailcnfg->mail_host,
                'port' => $mailcnfg->mail_port,
                'from' => array('address' => $mailcnfg->mail_username, 'name' => 'Admin'),
                'encryption' => $mailcnfg->mail_encryption,
                'username' => $mailcnfg->mail_username,
                'password' => $mailcnfg->mail_password,
                'sendmail' => '/usr/sbin/sendmail -bs',
                'pretend' => false
            );
            Config::set('mail', $config);
            $app = App::getInstance();
            $app->register('Illuminate\Mail\MailServiceProvider');
            $usrs = explode(',', $request['users']);
            $body = Emailtemplate::find($request['tmplt']);
            foreach ($usrs as $usr) {
                $contact = Contact::find($usr);
                // dd($body->description);
                if($contact->status == 1) {
                    $body->description = str_replace('{USER_NAME}', $contact->name, $body->description);
                    try {
                        Mail::to($contact->email)->send(new managermail($contact, $body));
                    } catch (Exception $ex) {
                        return redirect()->back()->withErrors("Something went wrong");
                    }
                }else{
                    $flg=1;
                }
            }
            if(count($usrs) == 1 && $flg == 1){
                return redirect()->back()->withErrors("User is not Active");
            }
            if(count($usrs) > 1 && $flg == 1){
                return redirect()->back()->withErrors("Some Users are not Active");
            }
            return redirect()->back()->withSuccess("Email Sent Successfully");
        }else{
            return redirect()->back()->withErrors("Please add SMTP details");
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
