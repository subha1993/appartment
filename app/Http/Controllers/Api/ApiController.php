<?php

namespace App\Http\Controllers\Api;

use App\PropertyManagement;
use App\Favourite;
use App\Image;
use App\User;
use App\Chat;
use App\Message;
use App\Pricerange;
use App\Neighborhood;
use App\Review;
use App\Fcmtable;
use App\Notification;
use App\VerificationDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Resetpassword;
class ApiController extends Controller
{
    public function __construct()
    {
       // $this->middleware('guest');
    }

    protected function encrypt($string, $key) {
        $result = '';
        for($i=0; $i<strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)+ord($keychar));
            $result.=$char;
        }
        return base64_encode($result);
    }

    protected function decrypt($string, $key){
        $result = '';
        $string = base64_decode($string);
        for($i=0; $i<strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)-ord($keychar));
            $result.=$char;
        }
        return $result;
    }

    public function sendNotification($tokens, $data, $notification=NULL)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'notification' => $notification,
            'registration_ids' => $tokens,
            'priority' => 'high',
            'data' => $data
        );

        $headers = array(
            'Authorization:key = '.env('FCM_SERVER_KEY'),
            'Content-Type: application/json'
        );

        return $this->getCurlData($url, json_encode($fields), $headers);
    }

    public function getCurlData($url, $poststr, $headers=NULL)
    {
        //dd($poststr);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl,CURLOPT_POSTFIELDS, $poststr);
        if($headers!=NULL){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        //curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $curlData = curl_exec($curl);
        //dd($curlData);
        if(curl_errno($curl))
        {
            return curl_error($curl);
        }
        else
        {
            curl_close($curl);
            return $curlData;
        }
    }


    public function propertylist(Request $request)
    {
        $prices=array();
        $input=$request->all();
        $device_id=$input['device_id'] ? $input['device_id'] : '';
        $dvc=$input['dvc'] ? $input['dvc'] : '';
        $page=$input['page'] ? $input['page'] : '';
        $perpage=$input['perpage'] ? $input['perpage'] : '';
//        if($request->has('bedroom')) {
//            $bedroom = $input['bedroom'] ? $input['bedroom'] : '';
//        }
        $latitude=$request->has('lat') ? $request->input('lat') : '';
        $longitude=$request->has('lng') ? $request->input('lng') : '';
        $bedroom = $request->has('bedroom') ? $request->input('bedroom') : '';
        $bathroom = $request->has('bathroom') ? $request->input('bathroom') : '';
        $area = $request->has('area') ? $request->input('area') : '';
        $minprice = $request->has('min_price') ? $request->input('min_price') : '';
        $maxprice = $request->has('max_price') ? $request->input('max_price') : '';
        $fromdate = $request->has('from_date') ? $request->input('from_date') : '';
        $todate = $request->has('to_date') ? $request->input('to_date') : '';
        $neighborhood = $request->has('neighborhood') ? $request->input('neighborhood') : '';
        $fcm = $request->has('fcmToken') ? $request->input('fcmToken') : '';
        $distance = env("distance");
        if($fcm != '') {
            $fcmt=Fcmtable::where(['user'=>$device_id])->first();
            if(empty($fcmt)) {
                Fcmtable::create([
                    'user' => $device_id,
                    'fcmtoken' => $fcm,
                ]);
            }else{
                $fcmt->fcmtoken=$fcm;
                $fcmt->save();
            }
        }
       // $prprts = DB::table("PropertyManagement");
        //        if($latitude != '' || $longitude != '') {
//            $prprts->whereRaw("SQRT( POW(69.1 * (loc_lat - " . $latitude . "), 2) + POW(69.1 * (" . $longitude . "- loc_long) * COS(loc_lat / 57.3), 2)) <=" . $distance);
//        }
        $prics=PropertyManagement::select('Rent')->get()->toArray();
        //dd($prices);
        foreach($prics as $prcs){
            if(!in_array($prcs['Rent'],$prices)) {
                array_push($prices, $prcs['Rent']);
            }
        }
        sort($prices);

        $prprts=PropertyManagement::where(['borough'=>'0']);
        if($neighborhood != ''){
            $prprts->whereIn('neighborhood', $neighborhood);
        }
        if($bedroom != ''){
            $prprts->whereIn('Bedrooms', $bedroom);
//            $prprts->where(['Bedrooms'=>$bedroom]);
        }
        if($bathroom != ''){
            $prprts->where(['Bathroom'=>$bathroom]);
        }
        if($area != ''){
            $prprts->where(['Room_size'=>$area]);
        }
        if($minprice != ''){
            $prprts->where('Rent','>=',$minprice);
        }
        if($maxprice != ''){
            $prprts->where('Rent','<=',$maxprice);
        }
        $prprts = $prprts->with(['image'])->get()->toArray();
        //  dd($prprts);
        // dd($prprts[0]['image']['image_name']);
        $latlng=array();
        $i=0;
        foreach($prprts as $key){
            $latlng[$i]['id']=$key['id'];
            $latlng[$i]['lat']=$key['loc_lat'];
            $latlng[$i]['lng']=$key['loc_long'];
            $latlng[$i]['bedroom']=$key['Bedrooms'];
            $latlng[$i]['bathroom']=$key['Bathroom'];
            if($key['Rent_currency_type'] == 'US$' || $key['Rent_currency_type'] == 'HK$'){
                $latlng[$i]['rent_currency']='$';
            }else{
                $latlng[$i]['rent_currency']=$key['Rent_currency_type'];
            }
            $latlng[$i]['rent']=$key['Rent'];
            $latlng[$i]['area']=$key['Room_size'];
            $latlng[$i]['img']=$key['image']['image_name'];
            $i++;
            if(!in_array($key['Rent'],$prices)) {
                array_push($prices, $key['Rent']);
            }
            sort($prices);
        }


         $properties=PropertyManagement::select('id','user_id','Bedrooms','Bathroom','Rent','Rent_currency_type','Room_size','unit_rent');
        if($neighborhood != ''){
            $properties->whereIn('neighborhood', $neighborhood);
        }
        if($bedroom != ''){
//            $properties->where(['Bedrooms'=>$bedroom]);
            $properties->whereIn('Bedrooms', $bedroom);
        }
        if($bathroom != ''){
            $properties->where(['Bathroom'=>$bathroom]);
        }
        if($area != ''){
            $properties->where(['Room_size'=>$area]);
        }
        if($minprice != ''){
            $properties->where('Rent','>=',$minprice);
        }
        if($maxprice != ''){
            $properties->where('Rent','<=',$maxprice);
        }
        $todaydate = date('Y-m-d');
        $todaydate=date('Y-m-d', strtotime($todaydate));
            $properties = $properties->with(['image','blockcount','favproperty'=>function($query)use($device_id){
                $query->where('device_id',$device_id);
            }])->orderBy('id','DESC')->paginate($perpage)->toArray();

        foreach ($properties['data'] as $key => $value) {
            if($value['Rent_currency_type'] == "US$" || $value['Rent_currency_type'] == "HK$"){
                $properties['data'][$key]['Rent_currency_type'] = '$';
            }
            if($value['unit_rent'] == '\month' || $value['unit_rent'] == '/month'){
                $properties['data'][$key]['unit_rent'] = 'per month';
            }
            if($value['image'] != null){
                $properties['data'][$key]['img']=$value['image']['image_name'];
            }else{
                $properties['data'][$key]['img']='';
            }
            if(!empty($value['favproperty'])){
                $properties['data'][$key]['fav']='1';
            }else{
                $properties['data'][$key]['fav']='2';
            }
            if(!empty($value['blockcount'])){
                $flag=0;
                foreach($value['blockcount'] as $blckcnt){
                    if (($todaydate >= $blckcnt['from_datetime']) && ($todaydate <= $blckcnt['to_datetime'])){
                        unset($properties['data'][$key]);
                       // $properties['data'][$key]['rmv']=1;
                        break;
                    }
                }
            }
            unset($properties['data'][$key]['image']);
            unset($properties['data'][$key]['favproperty']);
        }
        $prcrngs=Pricerange::all()->toArray();
        $minprc=array();
        $maxprc=array();
        foreach ($prcrngs as $val){
            if($val['status'] == 'l'){

                    array_push($maxprc, '<'.$val['minprice']);

            }
            elseif($val['status'] == 'g'){

                    array_push($minprc, $val['minprice'].'>');

            }else {
                if (!in_array($val['minprice'], $minprc)) {
                    array_push($minprc, $val['minprice']);
                }
                if (!in_array($val['maxprice'], $maxprc)) {
                    array_push($maxprc, $val['maxprice']);
                }
            }
        }
        sort($minprc);
        sort($maxprc);
        $properties['minprices']=$minprc;
        $properties['maxprices']=$maxprc;
        $properties['prices']=$prices;
        $notilist = Notification::select('id','chatId','propertyId','message','sender')->where(['receiver'=>$device_id])->groupBy('chatId')->orderBy('created_at','DESC')->get();
        $noticnt=count($notilist);

        $lgout=0;
        $usr=User::find($device_id);
        if(!empty($usr)){
          if($usr->device != $dvc){
              $lgout=1;
          }
        }
//        foreach ($properties['data'] as $key => $value) {
//            if($value['rmv'] == 1){
//                unset($properties['data'][$key]);
//            }
//        }
         
    //dd($properties);
        return response()->json([
            'logitout' => $lgout,
            'status' => 'success',
            'properties' => $properties,
            'noti_count'=>$noticnt,
            'latlngs' => $latlng,
        ], 200);
    }

  public function propertyDetails(Request $request){
        $input=$request->all();
        $device_id=$input['device_id'] ? $input['device_id'] : '';
        $id=$input['property_id'] ? $input['property_id'] : '';
        $property= PropertyManagement::where('id',$id)
            ->with(['images','propertyamenity.amenities','blockcount','favproperty'=>function($query)use($device_id){
                $query->where('device_id',$device_id);
            }])->first()->toArray();
      $property['Property_Description']=strip_tags($property['Property_Description']);
      $property['Property_Description']=mb_convert_encoding($property['Property_Description'], 'UTF-8', 'HTML-ENTITIES');
      if($property['Rent_currency_type'] == "US$" || $property['Rent_currency_type'] == "HK$"){
          $property['Rent_currency_type']='$';
      }
      if($property['unit_rent'] == '\month' || $property['unit_rent'] == '/month'){
          $property['unit_rent'] = 'per month';
      }
      if($property['display_address'] == null){
          $property['display_address'] = '';
      }
      if($property['Bills'] == null){
          $property['Bills'] = 'N/A';
      }
      if($property['Deposit'] == null){
          $property['Deposit'] = 'N/A';
      }
      if(!empty($property['favproperty'])){
          $property['fav']='1';
      }else{
          $property['fav']='2';
      }
      unset($property['favproperty']);
      foreach($property['propertyamenity'] as $key => $value){
          if($value['amenities']['root_id'] == 2){
              $property['propertyamenity'][$key]['root']='Unit Features';
          }
          elseif ($value['amenities']['root_id'] == 3){
              $property['propertyamenity'][$key]['root']='Pets';
          }
          else{
              $property['propertyamenity'][$key]['root']='';
          }
            $property['propertyamenity'][$key]['field']=$value['amenities']['fields'];
          $property['propertyamenity'][$key]['logo']=$value['amenities']['logo'];
            unset($property['propertyamenity'][$key]['amenities']);
        }
      $fvrt=Favourite::where('property_id',$id)->get()->toArray();
      $property['favcount']=count($fvrt);
      $property['reviewed'] = 0;
      $review=Review::where(['user_id'=>$device_id, 'apartment'=>$id])->get()->toArray();
      if(!empty($review)){
          $property['reviewed']=1;
      }

        return response()->json([
            'status' => 'success',
            'property' => $property,
        ], 200);
    }

    public function addFav(Request $request){
        $input=$request->all();
        if($input['favstatus'] == 1) {
            Favourite::create([
                'device_id' => $input['device_id'],
                'property_id' => $input['property_id'],
            ]);
            $fvrt=Favourite::where('property_id',$input['property_id'])->get()->toArray();
            $favcount=count($fvrt);
            $rspns='1';
        }elseif ($input['favstatus'] == 2){
            Favourite::where(['device_id'=>$input['device_id'], 'property_id'=>$input['property_id']])->delete();
            $fvrt=Favourite::where('property_id',$input['property_id'])->get()->toArray();
            $favcount=count($fvrt);
            $rspns='2';
        }else{
            $rspns='3';
        }
        return response()->json([
            'status' => $rspns,
            'favcount' => $favcount,
        ], 200);

    }

    public function Favlist(Request $request){
        $input=$request->all();
        $page=$input['page'] ? $input['page'] : '';
        $perpage=$input['perpage'] ? $input['perpage'] : '';
        $favs=Favourite::where('device_id',$input['device_id'])->get()->toArray();
        //  dd($favs);
        $prid=array();
        foreach($favs as $key) {
            $prid[]=$key['property_id'];
        }
        $properties=PropertyManagement::select('id','user_id','Bedrooms','Bathroom','Rent','Rent_currency_type','Room_size','unit_rent')->whereIn('id',$prid)->with(['image'])->paginate($perpage)->toArray();
        foreach ($properties['data'] as $key => $value) {
            if($value['Rent_currency_type'] == "US$" || $value['Rent_currency_type'] == "HK$"){
                $properties['data'][$key]['Rent_currency_type'] = '$';
            }
            if($value['unit_rent'] == '\month' || $value['unit_rent'] == '/month'){
                $properties['data'][$key]['unit_rent'] = 'per month';
            }
            if($value['image'] != null){
                $properties['data'][$key]['img']=$value['image']['image_name'];
            }else{
                $properties['data'][$key]['img']='';
            }
            unset($properties['data'][$key]['image']);
        }

        return response()->json([
            'status' => 'success',
            'favs' => $properties,
        ], 200);
    }

    public function managerlogin(Request $request){
        $input=$request->all();
        // $email=$input['email'];
        //  $password=$input['password'];
        $fcm=$request['fcmToken'];
        $device=$request['device_id'];
        $api_token=Str::random(60);
        $usr=User::where(['email' => $request['email'],'type' => 3])->first();
        $error_message='';
        $user='';
        $response='';
        if($usr != null) {
            if (Auth::attempt(['email' => $request['email'], 'password' => $request['password'], 'type' => 3])) {
                if(Auth::user()->is_active == 'N' ){
                    $response = 'N';
                    $error_message='Account is not Active';
                }
                elseif (Auth::user()->is_active == 'B'){
                    $response = 'N';
                    $error_message='Account is Blocked';
                }else {
                    Auth::user()->api_token = $api_token;
                    Auth::user()->device = $device;
                    Auth::user()->save();
                    $user = Auth::user();
                    $response = 'Y';
                    if($fcm != '') {
                        $fcmt=Fcmtable::where(['user'=>$usr->id])->first();
                        if(empty($fcmt)) {
                            Fcmtable::create([
                                'user' => $usr->id,
                                'fcmtoken' => $fcm,
                            ]);
                        }else{
                            if($fcmt->fcmtoken != $fcm) {
                                $token[] = $fcmt->fcmtoken;
                                //print_r($token);
                                $message = array(
                                    //sender: sender[0],
                                    'propertyId' => 0,
                                    'chatId' => 0,
                                    'message' => 'This User is logged in from another device',
                                    'noti_type' => 'logout',
                                    'noti_count' => 0
                                );
                                $data = array(
                                    'noti_type' => 'logout',
                                    //chatMessage: JSON.stringify({
                                    //message: {
                                    'message' => json_encode($message),

                                    // },
                                    // }),
                                    'badge' => '1',
                                    'click_action' => ".ResetPasswordActivity"
                                );
                                //$group = Group::where(['id'=>$val->group_id])->first();

                                //$noti = array("body" => $msg ,"title" => 'Ride has been requested in group '.$group->grp_name, "sound" => "default");
                                // dd($noti);
                                if (!empty($token)) {
                                    $message_status = $this->sendNotification($token, $data);
                                }
                            }
                            $fcmt->fcmtoken=$fcm;
                            $fcmt->save();
                        }
                    }
                }
            } else {
                $response = 'N';
                $error_message='Password is wrong';
            }
        }else{
            $response = 'N';
            $error_message='Email is wrong';
        }
        return response()->json([
            'status' => $response,
            'error_msg'=>$error_message,
            'user' => $user,
        ], 200);

    }

    public function managerapartments(Request $request){
       // dd($request['perpage']);
        $key=$request->has('keyword') ? $request->input('keyword') : '';
        $page=$request['page'] ? $request['page'] : '';
        $perpage=$request['perpage'] ? $request['perpage'] : '';
        $tkn=$request['api_token'];
        $usr=User::where('api_token',$tkn)->first();
       // dd($usr);
        if($usr != null){
            $property = PropertyManagement::where('user_id',$usr->id);
           // dd($property->get()->toArray());
            //dd($key);
            if( $key != '' ){
                //$property->where('display_Address','like',"%".$key."%")->orWhere(function()'Full_Address1','like',"%".$key."%")->orWhere('Full_Address2','like',"%".$key."%");
                $property->where(function($query) use($key){
                    $query->where('display_Address','like',"%".$key."%")
                        ->orWhere('Full_Address1','like',"%".$key."%")
                        ->orWhere('Full_Address2','like',"%".$key."%");
                });
            }
            $property=$property->with(['image'])->paginate($perpage)->toArray();
          //  dd($property);
            foreach ($property['data'] as $key => $value) {
                $property['data'][$key]['Property_Description']=strip_tags($value['Property_Description']);
                //$property[$key]['Property_Description']=mb_convert_encoding($value['Property_Description'], 'UTF-8', 'HTML-ENTITIES');
                if($value['Rent_currency_type'] == "US$" || $value['Rent_currency_type'] == "HK$"){
                    $property['data'][$key]['Rent_currency_type'] = '$';
                }
                if($value['image'] != null){
                    $property['data'][$key]['img']=$value['image']['image_name'];
                }else{
                    $property['data'][$key]['img']='';
                }
                unset($property['data'][$key]['image']);
               // unset($properties['data'][$key]['favproperty']);
            }
            $response='success';
        }else{
            $property='';
            $response='Unauthenticated';
        }
        if($response == 'Unauthenticated'){
            return response()->json([
                'status' => $response,
            ], 401);
        }else{
            return response()->json([
                'status' => $response,
                'properties' => $property,
            ], 200);
        }

    }

//    public function searchProperty(Request $request){
//        $key=$request['city'] ? $request['city'] : '';
//        $page=$request['page'] ? $request['page'] : '';
//        $perpage=$request['perpage'] ? $request['perpage'] : '';
//        $tkn=$request['api_token'];
//        $usr=User::where('api_token',$tkn)->first();
//        // dd($usr);
//        if($usr != null){
//            $property = PropertyManagement::where(['user_id'=>$usr->id]);
//            if($key != ''){
//                $property->where('city',$key);
//            }
//            $property=$property->with(['image'])->paginate($perpage)->toArray();
//            //  dd($property);
//            foreach ($property['data'] as $key => $value) {
//                $property['data'][$key]['Property_Description']=strip_tags($value['Property_Description']);
//                //$property[$key]['Property_Description']=mb_convert_encoding($value['Property_Description'], 'UTF-8', 'HTML-ENTITIES');
//                if($value['Rent_currency_type'] == "US$" || $value['Rent_currency_type'] == "HK$"){
//                    $property['data'][$key]['Rent_currency_type'] = '$';
//                }
//                if($value['image'] != null){
//                    $property['data'][$key]['img']=$value['image']['image_name'];
//                }else{
//                    $property['data'][$key]['img']='';
//                }
//                unset($property['data'][$key]['image']);
//                // unset($properties['data'][$key]['favproperty']);
//            }
//            $response='success';
//        }else{
//            $property='';
//            $response='Unauthenticated';
//        }
//        if($response == 'Unauthenticated'){
//            return response()->json([
//                'status' => $response,
//            ], 401);
//        }else{
//            return response()->json([
//                'status' => $response,
//                'properties' => $property,
//            ], 200);
//        }
//
//    }

    public function enquiry(Request $request){
//        $frmdate=$request->has('from_date') ? $request->input('from_date') : '';
//        $todate=$request->has('to_date') ? $request->input('to_date') : '';
        $device=$request->has('device_id') ? $request->input('device_id') : '';
        $mngr=$request->has('manager_id') ? $request->input('manager_id') : '';
        $apartment=$request->has('apartment') ? $request->input('apartment') : '';
        $property=PropertyManagement::where('id',$apartment)->with('image')->first();
        $manager=$property->user_id;
        if($property->Rent_currency_type == 'US$' || $property->Rent_currency_type == 'HK$'){
            $property->Rent_currency_type='$';
        }
        $property->img=$property->image->image_name;
        //dd($mngr);
//        if($manager != $mngr){
//            $mngr='';
//        }
        $message="Enquiry sent for the apartment ".$property->Bedrooms." Bedrooms with ".$property->Bathroom." Bathrooms, with the price ".$property->Rent_currency_type.''.$property->Rent.' '.$property->unit_rent;
        if($mngr == '') {
                $chat = Chat::where(['user_device' => $device, 'manager' => $manager, 'apartment' => $apartment])->first();
        }else{
            if($manager != $mngr){
                $chat = Chat::where(['user_device' => $mngr, 'manager' => $manager, 'apartment' => $apartment])->first();
            }else {
                $chat = Chat::where(['manager' => $mngr, 'apartment' => $apartment])->orderBy('created_at', 'DESC')->first();
            }
        }
        // dd($chat);
        $chtid=0;
        if(empty($chat)) {
            if($mngr != '' && $manager != $mngr){
                $ch = Chat::create([
                    'manager' => $manager,
                    'user_device' => $mngr,
                    'apartment' => $apartment,
                    'status' => 'Y'
                ]);
            }else {
                $ch = Chat::create([
                    'manager' => $manager,
                    'user_device' => $device,
                    'apartment' => $apartment,
                    'status' => 'Y'
                ]);
            }
            $chtid=$ch->id;
        }else{
            $chtid=$chat->id;
        }
//        if(empty($chat)) {
//            Message::create([
//                'chatId' => $chtid,
//                'sender' => $device,
//                'message' => $message,
//                'apartment' => $apartment,
//                'type' => 'M',
//                'status' => 'U',
//            ]);
//        }
        return response()->json([
            'status' => 'success',
            'chatid' => $chtid,
            'property'=>$property
        ], 200);
        //dd($property->Bedrooms);
//        $message=""


    }

    public function managerlogout(Request $request){
        $tkn=$request['api_token'];
        $usr=User::where('api_token',$tkn)->first();
        //dd($usr);
        if($usr != null){
            $usr->api_token='';
            $usr->save();
            $response='success';
        }else{
            $response='Unauthenticated';
        }
        if($response == 'Unauthenticated'){
            return response()->json([
                'status' => $response,
            ], 401);
        }else{
            return response()->json([
                'status' => $response,
            ], 200);
        }
    }

    public function allneighborhood(){
        $neighborhoods=Neighborhood::where('parent_id',1)->get()->toArray();
         usort($neighborhoods, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });


        return response()->json([
            'status' => 'success',
            'neighborhoods'=>$neighborhoods
        ], 200);
    }

    public function neighborhoods(){

        $neighborhoods=Neighborhood::where('parent_id',1)->get()->toArray();
        usort($neighborhoods, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });
        $downtown=array();
        $midtown=array();
        $eastside=array();
        $westside=array();
        $uppermanhattan=array();
       // dd($neighborhoods);
        foreach($neighborhoods as $key=>$value) {
           // if ($value['id'] == 11) {
                $hasprprt = PropertyManagement::where('neighborhood', $value['id'])->first();
              //  dd($hasprprt);
               if ($hasprprt != null) {
                    //  unset($neighborhoods[$key]);

                    if ($value['subneighbor'] == 1) {
                        array_push($downtown, $neighborhoods[$key]);
                    }
                    if ($value['subneighbor'] == 2) {
                        array_push($midtown, $neighborhoods[$key]);
                    }
                    if ($value['subneighbor'] == 3) {
                        array_push($eastside, $neighborhoods[$key]);
                    }
                    if ($value['subneighbor'] == 4) {
                        array_push($westside, $neighborhoods[$key]);
                    }
                    if ($value['subneighbor'] == 5) {
                        array_push($uppermanhattan, $neighborhoods[$key]);
                    }
               // }
            }
        }
        $alldt=array();
        $neighborhoods=array();
        $i=0;
        if(!empty($downtown)) {
            $neighborhoods[$i]['name'] = 'All Downtown';
            $neighborhoods[$i]['neighbor'] = $downtown;
            $i++;
        }
        if(!empty($midtown)) {
            $neighborhoods[$i]['name'] = 'All Midtown';
            $neighborhoods[$i]['neighbor'] = $midtown;
            $i++;
        }
        if(!empty($eastside)) {
            $neighborhoods[$i]['name'] = 'All Upper East Side';
            $neighborhoods[$i]['neighbor'] = $eastside;
            $i++;
        }
        if(!empty($westside)) {
            $neighborhoods[$i]['name'] = 'All Upper West Side';
            $neighborhoods[$i]['neighbor'] = $westside;
            $i++;
        }
        if(!empty($uppermanhattan)) {
            $neighborhoods[$i]['name'] = 'All Upper Manhattan';
            $neighborhoods[$i]['neighbor'] = $uppermanhattan;
            $i++;
        }
        $alldt['status']='success';
        $alldt['neighborhoods']=$neighborhoods;
        return response()->json($alldt, 200);
    }

    public function addreview(Request $request){
        $apartment=$request->has('property_id') ? $request->input('property_id') : '';
        $rating=$request->has('rating') ? $request->input('rating') : '';
        $review=$request->has('review') ? $request->input('review') : '';
        $device=$request->has('device_id') ? $request->input('device_id') : '';
        $name=$request->has('name') ? $request->input('name') : '';
        if($apartment == ''){
            return response()->json([
                'status' => 'Property_id not found',
            ], 400);
        }
//        if($rating == ''){
//            return response()->json([
//                'status' => 'Error',
//            ], 400);
//
//        }
//        else {
            Review::create([
                'user_id' => $device,
                'name' => $name,
                'rating' => $rating,
                'review' => $review,
                'apartment' => $apartment,
            ]);
            return response()->json([
                'status' => 'success',
            ], 200);
       // }

    }

    public function reviews(Request $request){
        $apartment=$request->has('property_id') ? $request->input('property_id') : '';
        $page=$request->has('page') ? $request->input('page') : '';
        $perpage=$request->has('perpage') ? $request->input('perpage') : '';
        $review= Review::where('apartment',$apartment)->orderBy('id','DESC')->paginate($perpage)->toArray();
        //dd($review);
        foreach($review['data'] as $key=>$val){
            $currentDateTime = $val['created_at'];
            $newDateTime = date('Y-m-d h:i:s A', strtotime($currentDateTime));
            $review['data'][$key]['created_at']=$newDateTime;
        }
        return response()->json([
            'status' => 'success',
            'reviews'=>$review
        ], 200);
    }

    public function chatlist(Request $request){
        $chk=$request['type'];
        //  if($chk == 'U') {
        $device_id = $request['device_id'];
//        }else{
//            $manager=$request['manager_id'];
//        }
//        'favproperty'=>function($query)use($device_id){
//            $query->where('device_id',$device_id);
//        }

        $page=$request->has('page') ? $request->input('page') : '';
        $perpage=$request->has('perpage') ? $request->input('perpage') : '';
        if($chk == 'U') {
//            $lastmsg = Message::whereHas('chats', function($query)use($device_id) {
//                $query->where('sender','!=',$device_id)->where('status','U');
//            })
//                ->orderBy('created_at', 'DESC')
//                ->get();
//            $chat = collect($lastmsg->groupBy('chatId'))->toArray();
//            dd($chat);

            $chat = Chat::where('user_device', $device_id)->with(['propertydetails.image','unreadmsg'=>function($query)use($device_id){$query->where('status','U')->where('sender','!=',$device_id);}])->orderBy('created_at','DESC')->paginate($perpage)->toArray();


        }else{
            $chat=Chat::where('manager',$device_id)->orWhere('user_device', $device_id)->with(['propertydetails.image','unreadmsg'=>function($query)use($device_id){$query->where('status','U')->where('sender','!=',$device_id);}])->orderBy('updated_at','DESC')->paginate($perpage)->toArray();
        }
        // dd($chat);
        $unchat=array();
        $rdchat=array();
        foreach ($chat['data'] as $key => $value) {
            if($value['propertydetails']['Rent_currency_type'] == "US$" || $value['propertydetails']['Rent_currency_type'] == "HK$"){
                $chat['data'][$key]['propertydetails']['Rent_currency_type'] = '$';
            }
            if($value['propertydetails']['image'] != null){
                $chat['data'][$key]['propertydetails']['img']=$value['propertydetails']['image']['image_name'];
            }else{
                $chat['data'][$key]['propertydetails']['img']='';
            }
            $chat['data'][$key]['pending']=count($value['unreadmsg']);

//            if(!empty($value['favproperty'])){
//                $properties['data'][$key]['fav']='1';
//            }else{
//                $properties['data'][$key]['fav']='2';
//            }
//            if(!empty($value['blockcount'])){
//                $flag=0;
//                foreach($value['blockcount'] as $blckcnt){
//                    if (($todaydate >= $blckcnt['from_datetime']) && ($todaydate <= $blckcnt['to_datetime'])){
//                        unset($properties['data'][$key]);
//                        // $properties['data'][$key]['rmv']=1;
//                        break;
//                    }
//                }
//            }
            unset($chat['data'][$key]['propertydetails']['image']);
            unset($chat['data'][$key]['unreadmsg']);

            if($chat['data'][$key]['pending'] > 0){
                array_push($unchat,$chat['data'][$key]);
            }else{
                array_push($rdchat,$chat['data'][$key]);
            }
            //unset($properties['data'][$key]['favproperty']);
        }
        $unchat=array_merge($unchat,$rdchat);
        $chat['data']=$unchat;
        return response()->json([
            'status' => 'success',
            'chats'=>$chat
        ], 200);
    }

    public function previouschat(Request $request){
        $chatId=$request['chatid'];
        $id=$request['id'];
        $mesg = Message::where(['chatId'=>$chatId])->where('sender','!=',$id)->update(['status'=>'R']);
        Notification::where(['chatId'=>$chatId])->where('sender','!=',$id)->delete();
        $page=$request->has('page') ? $request->input('page') : '';
        $perpage=$request->has('perpage') ? $request->input('perpage') : '';
        $msgs = Message::where('chatId',$chatId)->orderBy('id','DESC')->paginate($perpage)->toArray();
        foreach($msgs['data'] as $key=>$val){
//            $currentDateTime = $val['sendtime'];
//            $newDateTime = date('Y-m-d\Th:i:sZ', strtotime($currentDateTime));
//            $msgs['data'][$key]['created_at']=$newDateTime;
            $msgs['data'][$key]['created_at']=$val['sendtime'];
        }
        return response()->json([
            'status' => 'success',
           // 'pending' => '10',
            'messages'=>$msgs
        ], 200);
    }

    public function notifications(Request $request){
        $page=$request->has('page') ? $request->input('page') : '';
        $perpage=$request->has('perpage') ? $request->input('perpage') : '';
       // $chatid=$request->has('chatId') ? $request->input('chatId') : '';
        $device=$request->has('device_id') ? $request->input('device_id') : '';

//        $noti = DB::table('notifications')->select(
//                'notifications.id',
//                'notifications.chatId',
//                'notifications.propertyId',
//                'notifications.message',
//                'notifications.sender'
//            )
//            ->groupBy('notifications.chatId')
//            ->get()
//            ->toArray();
        $notilist = Notification::select('id','chatId','propertyId','message','sender','created_at')->where(['receiver'=>$device])->with(['propertyDetails.image'])->orderBy('created_at','DESC')->groupBy('chatId')->get()->toArray();
       // dd($notilist);
        foreach($notilist as $key=>$value){
            if($value['property_details']['Rent_currency_type'] == "US$" || $value['property_details']['Rent_currency_type'] == "HK$"){
                $notilist[$key]['property_details']['Rent_currency_type'] = '$';
            }
            if($value['property_details']['image'] != null){
                $notilist[$key]['property_details']['img']=$value['property_details']['image']['image_name'];
            }else{
                $notilist[$key]['property_details']['img']='';
            }
            unset($notilist[$key]['property_details']['image']);
        }
      //  $notilist = collect($lastmsg->groupBy('chatId'))->toArray();
//        $notilist = $lastmsg->groupBy('6');
        return response()->json([
            'status' => 'success',
            // 'pending' => '10',
            'notifications'=>$notilist
        ], 200);
    }

    public function forgetpassword(Request $request){
        $user=User::where('email',$request['email'])->first();
        //dd($user);
        if(!empty($user)) {
            $key = env("ENCRYPTION_KEY");
            $tkn = $this->encrypt($user->id, $key);
            $cnfrmlnk = "http://furnished.nyc/api/resetpassword/" . $tkn;
            try {
                Mail::to($user)->send(new Resetpassword($user, $confirm_link = $cnfrmlnk));
            }
            catch (\Exception $e){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Mail is not registered with aws',
                ], 200);
            }
        }

        if(empty($user)){
            return response()->json([
                'status' => 'error',
                'message' => 'User does not exist',
            ], 200);
        }else{
            return response()->json([
                'status' => 'success',
            ], 200);
        }
    }

    public function Resetpass($tkn){
        $key = env("ENCRYPTION_KEY");
        $id=$this->decrypt($tkn,$key);
        $usr=User::find($id);
        return response()->json([

            'status' => 'success',
        ], 200);

    }

    public function resetpassword(Request $request){
        $key = env("ENCRYPTION_KEY");
        $id=$this->decrypt($request['tkn'],$key);
        $usr=User::find($id);
        $usr->password=bcrypt($request['password']);
        $usr->save();
        if(empty($usr)){
            return response()->json([
                'status' => 'error',
                'message' => 'User does not exist',
            ], 200);
        }else{
            return response()->json([
                'status' => 'success',
            ], 200);
        }
    }

}