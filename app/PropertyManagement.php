<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyManagement extends Model
{
     protected $fillable = [
       'user_id', 'token', 'Property_title','Property_Description', 'price_currency_type', 'price',
       'Bedrooms', 'Bathroom', 'Location','loc_lat', 'loc_long', 'city',
       'country', 'borough', 'neighborhood', 'Full_Address1', 'Full_Address2','display_address','zipcode', 'Rent_currency_type', 'Rent',
       'unit_rent', 'Bills', 'Deposit','Room_size', 'imagecount', 'Block_time_count','nearest_busstop'
     ];
     
       public function image()
    {
        return $this->hasOne('App\Image', 'property_id', 'id');
    }
    
     public function images()
    {
        return $this->hasMany('App\Image', 'property_id', 'id');
    }

   /* public function amenities()
    {
        return $this->hasManyThrough('App\Amenity', 'App\Property_amenity');
    }*/

    public function propertyamenity()
    {
		return $this->hasMany('App\Property_amenity', 'property_id', 'id');
    }

    public function blockcount(){
        return $this->hasMany('App\Block_count', 'property_id', 'id');
    }

    public function favproperty(){
        return $this->hasMany('App\Favourite', 'property_id', 'id');
    }

    public function propertyuser(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
