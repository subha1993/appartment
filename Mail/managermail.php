<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Contact;

class managermail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var User
     */
    public $user;

    /**
     * The reset password link
     *
     * @var string
     */
    public $confirm_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact, $body)
    {
        //
        $this->contact = $contact;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->body->emailTitle)->markdown('admin.email.managermail', ['body' => $this->body]);
    }
}
